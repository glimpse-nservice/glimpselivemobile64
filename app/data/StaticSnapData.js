 const swotData ={
    "metrics": [
        {
            "key": "avgFrameUnitSaleGross",
            "dataType": "currency",
            "label": "Average Frame Sale",
            "tooltip": "Total Frame Production / Frame Units",
            
            "data": [
                {
                    "data": 181.93,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 181.93,
        
            "prev": [
                {
                    "data":  175.77,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal":  175.77,
         
            "growth": 0,
            "category": "Frames",
            "favorite": "0"
        },
        {
            "key": "sameDayCLCR",
            "dataType": "pct",
            "label": "Same Day Contact Lens Capture Rate",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 23.64,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 26.64,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 19.05,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 19.05,
            "lineItems": [],
            "orderBy": "999",
            "equation": "sum(<<sameDayContactLensCaptureRateUnits>>/<<contactLensFittings>>) * 100",
            "projectable": 0,
            "growth": 5,
            "category": "Contact Lens",
            "favorite": "0"
        },
        {
            "key": "revenuePerRefractionGross",
            "dataType": "currency",
            "label": "Revenue Per Refraction",
            "tooltip": "Production / Refractions",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 623.7,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 623.7,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 467.38,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 467.38,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce((sum(<<grossProduction>>) + sum(<<discounts>>))/sum(<<refractions>>),0)",
            "projectable": 0,
            "growth": 0,
            "category": "Financial",
            "favorite": "0"
        },
        {
            "key": "revenuePerPatientGross",
            "dataType": "currency",
            "label": "Revenue Per Patient",
            "tooltip": "Production / Unique Patients",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 426,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total":426,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 375,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 375,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<grossProduction>> + <<discounts>>)/<<distinctPatientCount>>,0)",
            "projectable": 0,
            "growth": 0,
            "category": "Financial",
            "favorite": "0"
        },
        {
            "key": "revenuePerEyewearPairGross",
            "dataType": "currency",
            "label": "Revenue Per Eyewear Pair",
            "tooltip": "Frames, Lenses, Lens Treatments, Lens Materials Production / Lens Pairs",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 108,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 108,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 100,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal":100,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<spectacleLensSalesGross>> + <<framesDollarsGross>>) / coalesce(NULLIF(sum(<<premiumProgressive>> + <<singleVision>> + <<bifocal>>  + <<trifocal>> + <<progressive>> + <<premiumSingle>>),0),1),0)",
            "projectable": 0,
            "growth": 8,
            "category": "Optical",
            "favorite": "0"
        },
        {
            "key": "revenuePerMedicalPatientGross",
            "dataType": "currency",
            "label": "Revenue Per Medical Patient",
            "tooltip": "Medical Revenue / Unique Medical Patients",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 3745.4,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 3745.4,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 3655.42,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal":  3655.42,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<medicalDollarsGross>>)/COALESCE(NULLIF(<<distinctMedicalPatientCount>>,0),1),0)",
            "projectable": 0,
            "growth": 0,
            "category": "Medical",
            "favorite": "0"
        },
        {
            "key": "medicalDollarsGross",
            "dataType": "currency",
            "label": "Medical Gross Billings",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 80.81,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 80.81,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 20.05,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 20.05,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<medicalDollarsGross>>),0)",
            "projectable": 1,
            "growth": 0,
            "category": "Medical",
            "favorite": "0"
        },
        {
            "key": "revenuePerCLFittingGross",
            "dataType": "currency",
            "label": "Revenue Per CL Fitting",
            "tooltip": "Contact Lenses Production / Contact Lens Fittings",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data":  268.98,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total":  268.98,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 214.74,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 214.74,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<contactLensDollarsGross>>)/sum(<<contactLensFittings>>),0)",
            "projectable": 0,
            "growth": 0,
            "category": "Contact Lens",
            "favorite": "0"
        },
        {
            "key": "newPatientPct",
            "dataType": "pct",
            "label": "% Of New Patients",
            "tooltip": "New Patient Exam Codes / Exam Codes",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 42.96,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 42.96,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 24.9,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 0,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<newPatients>>)/sum(<<existingPatients>> + <<newPatients>>),0) * 100",
            "projectable": 0,
            "growth": 0,
            "category": "Services",
            "favorite": "0"
        },
        {
            "key": "lensCount",
            "dataType": "units",
            "label": "Spectacle Lens Pairs",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 250,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 250,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 40,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 40,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<premiumProgressive>> + <<singleVision>> + <<bifocal>> + <<trifocal>> + <<progressive>> + <<premiumSingle>>),0)",
            "projectable": 1,
            "growth": 40,
            "category": "Optical",
            "favorite": "0"
        },
        {
            "key": "refractions",
            "dataType": "units",
            "label": "Refractions",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 310,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 310,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 197,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 197,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<refractions>>),0)",
            "projectable": 1,
            "growth": 0,
            "category": "Services",
            "favorite": "0"
        },
        {
            "key": "multiPairUnits",
            "dataType": "units",
            "label": "Multiple Lens Pair Units",
            "tooltip": "To qualify as a multi pair purchase, ALL pairs purchased need to come within the time frame selected.",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 34,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 34,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 20,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 20,
            "lineItems": [],
            "orderBy": "999",
            "equation": "<<multiPairUnits>>",
            "projectable": 1,
            "growth": 10,
            "category": "Optical",
            "favorite": "0"
        },
        {
            "key": "productionGross",
            "dataType": "currency",
            "label": "Gross Production",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 162314.73,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 162314.73,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 91459.46,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal":  91459.46,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<grossProduction>>) + sum(<<discounts>>),0)",
            "projectable": 1,
            "growth": 6,
            "category": "Payments",
            "favorite": "0"
        },
        {
            "key": "insurancePayments",
            "dataType": "currency",
            "label": "Insurance Payments",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 3,
            "currencyType": " ",
            "data": [
                {
                    "data": 400.58,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 400.58,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 300,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 300,
            "lineItems": [],
            "orderBy": "999",
            "equation": "SUM(<<insurancePayments>>)",
            "projectable": 0,
            "growth": 10,
            "category": "Payments",
            "favorite": "0"
        },
        {
            "key": "multiPairPct",
            "dataType": "pct",
            "label": "Multiple Lens Pair Pct",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 100,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 100,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 82,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 82,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce((sum(<<multiPairUnits>>)) / coalesce(NULLIF(sum(<<premiumProgressive>> + <<singleVision>> + <<bifocal>> + <<trifocal>> + <<progressive>> + <<premiumSingle>>),0),1),0) * 100",
            "projectable": 0,
            "growth": 20,
            "category": "Optical",
            "favorite": "0"
        },
        {
            "key": "payment",
            "dataType": "currency",
            "label": "Payments",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 3,
            "currencyType": " ",
            "data": [
                {
                    "data": 126852.15,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 126852.15,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 27475.99,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 27475.99,
            "lineItems": [],
            "orderBy": "999",
            "equation": "SUM(<<payments>>)",
            "projectable": 0,
            "growth": 20,
            "category": "Payments",
            "favorite": "0"
        },
        {
            "key": "frameUnits",
            "dataType": "units",
            "label": "Frame Units",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 35,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 35,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 40,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 40,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<frameUnits>>),0)",
            "projectable": 1,
            "growth": -5,
            "category": "Frames",
            "favorite": "0"
        },
        {
            "key": "framesDollarsGross",
            "dataType": "currency",
            "label": "Frame Sales",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 20741,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 20741,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 14190,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 14190,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<framesDollarsGross>>),0)",
            "projectable": 1,
            "growth": 5,
            "category": "Frames",
            "favorite": "0"
        },
        {
            "key": "contactLensFittings",
            "dataType": "units",
            "label": "Contact Lens Fittings",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 63,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 63,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 39,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 39,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<contactLensFittings>>),0)",
            "projectable": 1,
            "growth": 10,
            "category": "Contact Lens",
            "favorite": "0"
        },
        {
            "key": "patientPayments",
            "dataType": "currency",
            "label": "Patient Payments",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 3,
            "currencyType": " ",
            "data": [
                {
                    "data": 77.63,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 77.63,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 16.51,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 16.51,
            "lineItems": [],
            "orderBy": "999",
            "equation": "SUM(<<patientPayments>>)",
            "projectable": 0,
            "growth": 0,
            "category": "Payments",
            "favorite": "0"
        },
        {
            "key": "frameCaptureRate",
            "dataType": "pct",
            "label": "Frame Capture Rate",
            "tooltip": "Frames / Refractions",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 43.38,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 43.38,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 40,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 40,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<frameUnits>>)/sum(<<refractions>>),0) * 100",
            "projectable": 0,
            "growth": 0,
            "category": "Frames",
            "favorite": "0"
        },
        {
            "key": "contactLensDollarsGross",
            "dataType": "currency",
            "label": "Contact Lens",
            "tooltip": "Contact Lens Revenue + Evaluations",
            "trend": 1,
            "verifeye": 0,
            "featureId": 4,
            "currencyType": "Gross",
            "data": [
                {
                    "data":  18828.82,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total":  18828.82,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data":  9019,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 9019,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<contactLensDollarsGross>>),0)",
            "projectable": 1,
            "growth": 1,
            "category": "Contact Lens",
            "favorite": "0"
        },
        {
            "key": "opticalCaptureRate",
            "dataType": "pct",
            "label": "Lens Capture Rate",
            "tooltip": "Lens Pairs / Refractions",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 80.81,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 80.81,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 90.5,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 90.5,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<premiumProgressive>> + <<singleVision>> + <<bifocal>> + <<trifocal>> + <<progressive>> + <<premiumSingle>>  ) / coalesce(NULLIF(sum(<<refractions>>),0),1),0) * 100",
            "projectable": 0,
            "growth": -5,
            "category": "Optical",
            "favorite": "0"
        },
        {
            "key": "contactLensCaptureRate",
            "dataType": "pct",
            "label": "Contact Lens Capture Rate",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 29.63,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 29.63,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 20.64,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 20.64,
            "lineItems": [],
            "orderBy": "999",
            "equation": "(sum(<<contactLensCaptureRate>>)/coalesce(NULLIF(sum(<<contactLensFittingsCaptureRate>>),0),1)) * 100",
            "projectable": 0,
            "growth":9,
            "category": "Contact Lens",
            "favorite": "0"
        },
        {
            "key": "sameDayMultiPairPct",
            "dataType": "pct",
            "label": "Same Day Multiple Lens Pair Pct",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": " ",
            "data": [
                {
                    "data": 100,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total":100,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 82,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 82,
            "lineItems": [],
            "orderBy": "999",
            "equation": "(<<sameDayMultiPairUnits>>/<<multiPairUnits>>) * 100",
            "projectable": 0,
            "growth":20,
            "category": "Optical",
            "favorite": "0"
        },
        {
            "key": "spectacleLensSalesGross",
            "dataType": "currency",
            "label": "Spectacle Lens Sales",
            "tooltip": "",
            "trend": 1,
            "verifeye": 0,
            "featureId": 0,
            "currencyType": "Gross",
            "data": [
                {
                    "data": 27197,
                    "label": "3/5/2019-3/5/2019",
                    "groupId": "",
                    "code": ""
                }
            ],
            "total": 27197,
            "benchmarkTotal": 0,
            "prev": [
                {
                    "data": 14350,
                    "label": "3/5/2018-3/5/2018",
                    "groupId": "",
                    "code": ""
                }
            ],
            "prevTotal": 14350,
            "lineItems": [],
            "orderBy": "999",
            "equation": "coalesce(sum(<<spectacleLensSalesGross>>),0)",
            "projectable": 1,
            "growth":5,
            "category": "Optical",
            "favorite": "0"
        }
    ],
}

export default swotData