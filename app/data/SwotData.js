 const swotData ={
  "data": [
    {
        "practiceId": "",
        "practiceName": "",
        "locationId": "3735",
        "locationName": "Location #1",
        "strengths": [
            {
                "metricName": "Revenue Per Eyewear Pair",
                "metricsTemplateId": 308,
                "metricSymbol": "currency",
                "metricValue": null,
                "metricData": 400.00,
                "metricPrevData": 0,
                "decimalRanking": 10.3853820598,
                "metricRanking": "Top 20%",
                "metricContent": "Your practice is doing an excellent job at maximizing revenue per eyewear. Keep up the good work to continue growth in your practice.",
                "metricGrowth": 0
            },
        ],
        "weaknesses": [
       
        
       
            {
                "metricName": "Average Frame Sales",
                "metricsTemplateId": 296,
                "metricSymbol": "currency",
                "metricValue": null,
                "metricData": 30.00,
                "metricPrevData": 0,
                "decimalRanking": 84.05315614617,
                "metricRanking": "Bottom 20%",
                "metricContent": "It may be time to have a team meeting to discuss increasing your average frame sale. Have you brought in any new frame lines within the past 6 months? Use the power of \"NEW\" to give frames a boost.  Make sure to view your FRAME report that provides you a break down of frames sold by price points.  Mini Games may be a great option for you to set individual goals for your team.",
                "metricGrowth": 0
            },
            {
                "metricName": "Revenue Per Contact Lens Fitting",
                "metricsTemplateId": 291,
                "metricSymbol": "currency",
                "metricValue": null,
                "metricData": 500,
                "metricPrevData": 0,
                "decimalRanking": 95.65217391304,
                "metricRanking": "Bottom 10%",
                "metricContent": "It may be time to have a team meeting to discuss how to increase your revenue per cl fitting. You may want to announce new technologies and get your patients excited to try a new contact lens. Researchers have found that daily disposable contact lenses have a much higher compliance rate. Contact lens sales could be a significant revenue source for your practice.  View your doctor comparison report to check out individual doctor's contact lens capture rate to set individual goals.",
                "metricGrowth": 0
            }
        ],
        "opportunities": [
            {
                "metricName": "Frame Capture Rate",
                "metricsTemplateId": 27,
                "metricSymbol": "pct",
                "metricValue": null,
                "metricData": 300,
                "metricPrevData": 0,
                "decimalRanking": 50.0,
                "metricRanking": "Bottom 50%",
                "metricContent": "You are doing well, but there may be more opportunities to improve your handoff and educate your patients on product features and benefits. Patients are more likely to buy with a doctor recommendation. Data alone is meaningful, but only if you use the information to create and execute a capture rate improvement plan.",
                "metricGrowth": 0
            },
        ],
        "threats": []
    }
]
}

export default swotData