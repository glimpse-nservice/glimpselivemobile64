const demoData = {

    "newPatientPct": [{
        "metrics": [
            {
                "key": "newPatientPct",
                "dataType": "pct",
                "label": "% Of New Patients",


                "total": 42.96,
                "prevTotal": 24.9,

                "growth": 5,

                "data": [

                    {
                        "data": 42.96,
                        "label": "Jan-2019",
                        "groupId": "",

                    },
                    {
                        "data": 42.96,
                        "label": "Feb-2019",
                        "groupId": "",

                    },

                    {
                        "data": 0,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }




                ],
                "prev": [
                    {
                        "data": 24.9,
                        "label": "Jan-2018",
                        "groupId": "",
                    },
                    {
                        "data": 24.9,
                        "label": "Feb-2018",
                        "groupId": "",
                    },
                    {
                        "data": 0,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ]

            }
        ],

    }],
    "contactLensDollarsGross": [{
        "metrics": [
            {
                "key": "contactLensDollarsGross",
                "dataType": "currency",
                "label": "Contact Lens",


                "total": 18828.82,
                "prevTotal": 9019,

                "growth": 3,

                "data": [

                    {
                        "data": 18828.82,
                        "label": "Jan-2019",
                        "groupId": "",
                    },
                    {
                        "data": 18528.82,
                        "label": "Feb-2019",
                        "groupId": "",
                    },

                    {
                        "data": 18528.82,
                        "label": "March-2019",
                        "groupId": "",
                    }


                ],
                "prev": [
                    {
                        "data": 9019,
                        "label": "Jan-2018",
                        "groupId": "",
                    },
                    {
                        "data": 9019,
                        "label": "Feb-2018",
                        "groupId": "",
                    },
                    {
                        "data": 0,
                        "label": "March-2018",
                        "groupId": ""
                    }
                ]

            }
        ],
    }],
    "refractions": [{
        "metrics": [
            {
                "key": "refractions",
                "dataType": "units",
                "label": "Refractions",


                "total": 310,
                "prevTotal": 197,

                "growth": 0,

                "data": [

                    {
                        "data": 310,
                        "label": "Jan-2019",
                        "groupId": "",
                    },
                    {
                        "data": 330,
                        "label": "Feb-2019",
                        "groupId": "",
                    },

                    {
                        "data": 320,
                        "label": "March-2019",
                        "groupId": "",
                    }


                ],
                "prev": [
                    {
                        "data": 197,
                        "label": "Jan-2018",
                        "groupId": "",
                    },
                    {
                        "data": 200,
                        "label": "Feb-2018",
                        "groupId": "",
                    },
                    {
                        "data": 300,
                        "label": "March-2018",
                        "groupId": ""
                    }
                ]

            }
        ],

    }],
    "frameCaptureRate": [{
        "metrics": [
            {
                "key": "frameCaptureRate",
                "dataType": "pct",
                "label": "Frame Capture Rate",
                "data": [
                    {
                        "data": 43.38,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 43.38,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 41.50,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 43.38,
                "prevTotal": 40,
                "prev": [
                    {
                        "data": 40,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 40,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 38,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],

                "growth": -7,

            }
        ],

    }],
    "revenuePerPatientGross": [{
        "metrics": [
            {
                "key": "revenuePerPatientGross",
                "dataType": "currency",
                "label": "Revenue Per Patient",
                "data": [
                    {
                        "data": 426,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 400,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 380,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 426,
                "prevTotal": 375,
                "prev": [
                    {
                        "data": 375,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 370,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 300,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],

                "growth": 7,

            }
        ],

    }],
    "revenuePerRefractionGross": [{
        "metrics": [
            {
                "key": "revenuePerRefractionGross",
                "dataType": "currency",
                "label": "Revenue Per Refraction",



                "data": [
                    {
                        "data": 623.7,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 650,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 600,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 623.7,

                "prev": [
                    {
                        "data": 467.38,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 400.50,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 390.0,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 467.38,

                "growth": 1,

            }
        ],

    }],
    "multiPairUnits": [{
        "metrics": [
            {
                "key": "multiPairUnits",
                "dataType": "units",
                "label": "Multiple Lens Pair Units",

                "data": [
                    {
                        "data": 34,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 30,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 10,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 34,

                "prev": [
                    {
                        "data": 20,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 20,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 5,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 20,

                "growth": 14,

            }
        ],
    }],
    "spectacleLensSalesGross": [{
        "metrics": [
            {
                "key": "spectacleLensSalesGross",
                "dataType": "currency",
                "label": "Spectacle Lens Sales",

                "data": [
                    {
                        "data": 27197,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 2500,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 1980,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 27197,

                "prev": [
                    {
                        "data": 14350,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 15000,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 2000,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 14350,

                "growth": 12.5,


            }
        ],
    }],
    "medicalDollarsGross": [{
        "metrics": [
            {
                "key": "medicalDollarsGross",
                "dataType": "currency",
                "label": "Medical Gross Billings",

                "data": [
                    {
                        "data": 80.79,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 50.30,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 80.0,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 80.81,

                "prev": [
                    {
                        "data": 20.5,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 50.4,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 20.5,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 20.5,

                "growth": 4,

            }
        ],
    }],
    "revenuePerMedicalPatientGross": [{
        "metrics": [
            {
                "key": "revenuePerMedicalPatientGross",
                "dataType": "currency",
                "label": "Revenue Per Medical Patient",

                "data": [
                    {
                        "data": 3745.4,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 3000.5,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 2000,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 3745.4,

                "prev": [
                    {
                        "data": 3655.42,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 3655.42,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 3655.42,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 3655.42,

                "growth": 4,

            }
        ],
    }],
    "contactLensFittings": [{
        "metrics": [
            {
                "key": "contactLensFittings",
                "dataType": "units",
                "label": "Contact Lens Fittings",

                "data": [
                    {
                        "data": 63,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 50,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 60,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 63,

                "prev": [
                    {
                        "data": 39,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 30,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 29,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 39,

                "growth": 10,

            }
        ],
    }],
    "revenuePerCLFittingGross": [{
        "metrics": [
            {
                "key": "revenuePerCLFittingGross",
                "dataType": "currency",
                "label": "Revenue Per CL Fitting",

                "data": [
                    {
                        "data": 268.98,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 298.98,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 168.98,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 268.98,

                "prev": [
                    {
                        "data": 214.74,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 254.74,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 214.74,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],

                "growth": 4,

            }
        ],
    }],
    "sameDayCLCR": [{
        "metrics": [
            {
                "key": "sameDayCLCR",
                "dataType": "pct",
                "label": "Same Day Contact Lens Capture Rate",

                "data": [
                    {
                        "data": 23.64,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 20.5,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 22.5,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 23.64,

                "prev": [
                    {
                        "data": 19.05,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 19.05,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 10.05,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 19.05,

                "growth": 9,

            }
        ],
    }],
    "contactLensCaptureRate": [{
        "metrics": [
            {
                "key": "contactLensCaptureRate",
                "dataType": "pct",
                "label": "Contact Lens Capture Rate",

                "data": [
                    {
                        "data": 29.63,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 40.58,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 30.78,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 29.63,

                "prev": [
                    {
                        "data": 20.64,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 19.78,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 22.64,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "prevTotal": 20.64,

                "growth": 9.5,

            }
        ],
    }]
}


export default demoData