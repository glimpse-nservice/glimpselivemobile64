const demoData = {

    "newPatientPct": [{
        "metrics": [
            {
                "key": "newPatientPct",
                "dataType": "pct",
                "label": "% Of New Patients",
                "favorite": "1",

                "total": 42.96,
                "prevTotal": 24.9,

                "growth": 5,

                "data": [

                    {
                        "data": 42.96,
                        "label": "Jan-2019",
                        "groupId": "",

                    },
                    {
                        "data": 42.96,
                        "label": "Feb-2019",
                        "groupId": "",

                    },

                    {
                        "data": 0,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }




                ],
                "prev": [
                    {
                        "data": 24.9,
                        "label": "Jan-2018",
                        "groupId": "",
                    },
                    {
                        "data": 24.9,
                        "label": "Feb-2018",
                        "groupId": "",
                    },
                    {
                        "data": 0,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ]

            }
        ],

    }],
    "contactLensDollarsGross": [{
        "metrics": [
            {
                "key": "contactLensDollarsGross",
                "dataType": "currency",
                "label": "Contact Lens",
                "favorite": "1",

                "total": 18828.82,
                "prevTotal": 9019,

                "growth": 3,

                "data": [

                    {
                        "data": 18828.82,
                        "label": "Jan-2019",
                        "groupId": "",
                    },
                    {
                        "data": 18528.82,
                        "label": "Feb-2019",
                        "groupId": "",
                    },

                    {
                        "data": 18528.82,
                        "label": "March-2019",
                        "groupId": "",
                    }


                ],
                "prev": [
                    {
                        "data": 9019,
                        "label": "Jan-2018",
                        "groupId": "",
                    },
                    {
                        "data": 9019,
                        "label": "Feb-2018",
                        "groupId": "",
                    },
                    {
                        "data": 0,
                        "label": "March-2018",
                        "groupId": ""
                    }
                ]

            }
        ],
    }],
    "refractions": [{
        "metrics": [
            {
                "key": "refractions",
                "dataType": "units",
                "label": "Refractions",

                "favorite": "1",
                "total": 310,
                "prevTotal": 197,

                "growth": 0,

                "data": [

                    {
                        "data": 310,
                        "label": "Jan-2019",
                        "groupId": "",
                    },
                    {
                        "data": 330,
                        "label": "Feb-2019",
                        "groupId": "",
                    },

                    {
                        "data": 320,
                        "label": "March-2019",
                        "groupId": "",
                    }


                ],
                "prev": [
                    {
                        "data": 197,
                        "label": "Jan-2018",
                        "groupId": "",
                    },
                    {
                        "data": 200,
                        "label": "Feb-2018",
                        "groupId": "",
                    },
                    {
                        "data": 300,
                        "label": "March-2018",
                        "groupId": ""
                    }
                ]

            }
        ],

    }],
    "frameCaptureRate": [{
        "metrics": [
            {
                "key": "frameCaptureRate",
                "dataType": "pct",
                "label": "Frame Capture Rate",
                "favorite": "1",
                "data": [
                    {
                        "data": 43.38,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 43.38,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 41.50,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 43.38,
                "prevTotal": 40,
                "prev": [
                    {
                        "data": 40,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 40,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 38,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],

                "growth": -7,

            }
        ],

    }],
    "revenuePerPatientGross": [{
        "metrics": [
            {
                "key": "revenuePerPatientGross",
                "dataType": "currency",
                "label": "Revenue Per Patient",
                "favorite": "1",
                "data": [
                    {
                        "data": 426,
                        "label": "Jan-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 400,
                        "label": "Feb-2019",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 380,
                        "label": "Mar-2019",
                        "groupId": "",
                        "code": ""
                    }
                ],
                "total": 426,
                "prevTotal": 375,
                "prev": [
                    {
                        "data": 375,
                        "label": "Jan-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 370,
                        "label": "Feb-2018",
                        "groupId": "",
                        "code": ""
                    },
                    {
                        "data": 300,
                        "label": "Mar-2018",
                        "groupId": "",
                        "code": ""
                    }
                ],

                "growth": 7,

            }
        ],

    }],
    
        
    
}


export default demoData