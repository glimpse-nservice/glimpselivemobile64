 const oppData ={
 "Clincial":  [
        {
            
            "category": "Clinical",
            "opportunity": 20,
            "performance": 10,
            "opportunities": [
                {
                    "metricName": "Revenue Per Refraction",
                    "metricKey": "revenuePerRefractionGross",
                    "performance": 454.15,
                    "comparison": 554.15,
                    "missedOpportunity": 10,
                    "dataType": "currency",
                    "difference": 554.15,
                    "benchmarkCR": 554.15,
                    "denominator": 0,
                    "clientUnits": 0,
                    "clientAverageDollars": 0,
                    "potential": 0,
                    "frameDollars": 0,
                    "lensDollars": 0
                },
                {
                    "metricName": "Revenue Per Medical Patient",
                    "metricKey": "revenuePerMedicalPatientGross",
                    "performance": 0,
                    "comparison": 341.36,
                    "missedOpportunity": 0,
                    "dataType": "currency",
                    "difference": 341.36,
                    "benchmarkCR": 341.36,
                    "denominator": 0,
                    "clientUnits": 0,
                    "clientAverageDollars": 0,
                    "potential": 0,
                    "frameDollars": 0,
                    "lensDollars": 0
                }
            ]
        }
    ],
"ContactLens":[
    {
        "practiceId": "2966",
        "practiceName": "",
        "locationId": "3735",
        "locationName": "Location #1",
        "category": "Contact Lens",
        "opportunity": 10.5,
        "performance": 45,
        "opportunities": [
            {
                "metricName": "Contact Lens Capture Rate",
                "metricKey": "contactLensCaptureRate",
                "performance": 30,
                "comparison": 36.43,
                "missedOpportunity": 0,
                "dataType": "pct",
                "difference": 0,
                "benchmarkCR": 36.43,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            },
            {
                "metricName": "Daily Disposable Pct",
                "metricKey": "cldailypct",
                "performance": 0,
                "comparison": 54.18,
                "missedOpportunity": 0,
                "dataType": "pct",
                "difference": 0,
                "benchmarkCR": 54.18,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            },
            {
                "metricName": "Revenue Per CL Fitting",
                "metricKey": "revenuePerCLFittingGross",
                "performance": 0,
                "comparison": 341.83,
                "missedOpportunity": 0,
                "dataType": "currency",
                "difference": 341.83,
                "benchmarkCR": 341.83,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            }
        ]
    }
],
"OfficeVisits":[
    {
        "practiceId": "2966",
        "practiceName": "",
        "locationId": "3735",
        "locationName": "Location #1",
        "category": "Office Visits",
        "opportunity": 50,
        "performance": 45,
        "opportunities": [
            {
                "metricName": "Fundus Photography",
                "metricKey": "fundusPhotography",
                "performance": 77,
                "comparison": 77,
                "missedOpportunity": 0,
                "dataType": "units",
                "difference": 77,
                "benchmarkCR": 77,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 80,
                "frameDollars": 0,
                "lensDollars": 0
            },
            {
                "metricName": "Fundus photography screening",
                "metricKey": "fundusScreenings",
                "performance": 0,
                "comparison": 155,
                "missedOpportunity": 0,
                "dataType": "units",
                "difference": 155,
                "benchmarkCR": 155,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            },
            {
                "metricName": "Scanning diagnostic imaging, retina",
                "metricKey": "octRetina",
                "performance": 0,
                "comparison": 18,
                "missedOpportunity": 0,
                "dataType": "units",
                "difference": 18,
                "benchmarkCR": 18,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            }
        ]
    }
],
"Optical":[
    {
        "practiceId": "2966",
        "practiceName": "",
        "locationId": "3735",
        "locationName": "Location #1",
        "category": "Optical",
        "opportunity": 70,
        "performance": 70,
        "opportunities": [
            {
                "metricName": "Frame Capture Rate",
                "metricKey": "frameCaptureRate",
                "performance": 55,
                "comparison": 44.1,
                "missedOpportunity": 0,
                "dataType": "pct",
                "difference": 11.1,
                "benchmarkCR": 44.1,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential":50,
                "frameDollars": 0,
                "lensDollars": 0
            },
            {
                "metricName": "Lens Capture Rate",
                "metricKey": "opticalCaptureRate",
                "performance": 50,
                "comparison": 47.23,
                "missedOpportunity": 0,
                "dataType": "pct",
                "difference": 0,
                "benchmarkCR": 47.23,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            },
            {
                "metricName": "Average Frame Sale",
                "metricKey": "avgFrameUnitSaleGross",
                "performance": 200.59,
                "comparison": 184.59,
                "missedOpportunity": 0,
                "dataType": "currency",
                "difference": 184.59,
                "benchmarkCR": 184.59,
                "denominator": 0,
                "clientUnits": 0,
                "clientAverageDollars": 0,
                "potential": 0,
                "frameDollars": 0,
                "lensDollars": 0
            }
        ]
    }
]
}

export default oppData