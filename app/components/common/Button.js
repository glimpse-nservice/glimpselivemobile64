import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Constants from '../../config/Constants';

const Button = ({ onPress, children, width, iconValue }) => {
  const { button, text, icon } = styles;
  return (
    <TouchableOpacity style={button} onPress={onPress}>
      <Text style={icon}>{iconValue}</Text>
      <Text style={[text, { width }]}>{children}</Text>
    </TouchableOpacity>
  );
};

const styles = {
  button: {
    marginTop: 16,
    marginLeft: 24,
    marginRight: 24,
    backgroundColor: '#82838c',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 25
  },
  text: {
    marginLeft: 16,
    color: 'white',
    fontFamily: 'Roboto-Regular',
    fontWeight: '500'
  },
  icon: {
    fontSize: 24,
    fontFamily: 'icomoon',
    color: 'white'
  }
};

export { Button };
