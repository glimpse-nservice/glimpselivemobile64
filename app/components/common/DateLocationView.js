import React, { Component } from "react";
import ModalSelector from "react-native-modal-selector";
import { View, Dimensions, Text, TouchableWithoutFeedback } from "react-native";
import { dateSet, getLocationSet } from "../../config/Utility";
import { UserData } from "../../config/UserData";

let locationSet = [];

class DateLocationView extends Component {
  constructor(props) {
    super(props);

    this.state = { selectedDate: dateSet[5], selectedLocation: " " };

    UserData.getUserLocations()
      .then(res => {
        locationSet = getLocationSet(res);

        this.setState(
          {
            selectedLocation: locationSet[0]
          },
          () => {
            this.props.onDateOrLocationSet(
              this.state.selectedDate,
              this.state.selectedLocation.value
            );
          }
        );
      })
      .catch(message => alert(message));
  }

  UNSAFE_componentWillReceiveProps(props) {
    var datePos = props.labelDate;
    var locationPos = locationSet.filter(elements => {
      return elements.value === props.labelLoc;
    });
    this.setState({
      selectedLocation: locationSet[locationPos[0].key],
      selectedDate: dateSet[datePos.key]
    });
  }

  render() {
    return (
      <View>
        <View style={{ backgroundColor: "#82838c", height: 40 }} />
        <View style={{ marginTop: -40 }}>
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            {this.renderDateHeader()}
            {this.renderLocationHeader()}
          </View>
          <View style={{ flexDirection: "row", justifyContent: "center" }}>
            {this.renderDateDropDown()}
            {this.renderLocationDropDown()}
          </View>
        </View>
      </View>
    );
  }

  /**
   * This function renders the header for the date drop down
   */
  renderDateHeader() {
    const showDate =
      this.props.showDate === undefined ? true : this.props.showDate;

    if (showDate) {
      return (
        <Text style={[styles.selectDate, { marginLeft: 8, marginRight: 16 }]}>
          Select Date
        </Text>
      );
    }
  }

  /**
   * This function renders the header for the location drop down
   */
  renderLocationHeader() {
    const showLocation =
      this.props.showLocation === undefined ? true : this.props.showLocation;

    if (showLocation) {
      return (
        <Text style={[styles.selectDate, { marginLeft: 8, marginRight: 16 }]}>
          Select Location
        </Text>
      );
    }
  }

  /**
   * This function render drop down for date
   */
  renderDateDropDown() {
    const showDate =
      this.props.showDate === undefined ? true : this.props.showDate;

    if (showDate) {
      return (
        <TouchableWithoutFeedback
          onPress={() => this.dateSelector.open()}
          disabled={this.props.disabled}
        >
          <View style={[styles.date, { marginLeft: 8, marginRight: 16 }]}>
            <ModalSelector
              data={dateSet}
              ref={selector => {
                this.dateSelector = selector;
              }}
              initValue="Select a date"
              accessible
              disabled={this.props.disabled}
              scrollViewAccessibilityLabel={"Scrollable options"}
              cancelButtonAccessibilityLabel={"Cancel Button"}
              animationType="none"
              cancelText="Cancel"
              onChange={option => {
                this.setState(
                  {
                    selectedDate: option
                  },
                  () => {
                    this.props.onDateOrLocationSet(
                      this.state.selectedDate,
                      this.state.selectedLocation.value
                    );
                  }
                );
              }}
            >
              <Text
                style={{
                  margin: 10,
                  color: "#394263",
                  fontFamily: "Roboto-Bold"
                }}
              >
                {this.state.selectedDate.label}
              </Text>
            </ModalSelector>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }

  /**
   * This function render drop down for location
   */
  renderLocationDropDown() {
    const showLocation =
      this.props.showLocation === undefined ? true : this.props.showLocation;

    if (showLocation) {
      return (
        <TouchableWithoutFeedback
          onPress={() => this.locationSelector.open()}
          disabled={this.props.disabled}
        >
          <View style={[styles.date, { marginLeft: 8, marginRight: 16 }]}>
            <ModalSelector
              data={locationSet}
              ref={selector => {
                this.locationSelector = selector;
              }}
              initValue="Select a location"
              accessible
              disabled={this.props.disabled}
              scrollViewAccessibilityLabel={"Scrollable options"}
              cancelButtonAccessibilityLabel={"Cancel Button"}
              animationType="none"
              cancelText="Cancel"
              onChange={option => {
                this.setState(
                  {
                    selectedLocation: option
                  },
                  () => {
                    this.props.onDateOrLocationSet(
                      this.state.selectedDate,
                      this.state.selectedLocation.value
                    );
                  }
                );
              }}
            >
              <Text
                style={{
                  margin: 10,
                  color: "#394263",
                  fontFamily: "Roboto-Bold"
                }}
              >
                {this.state.selectedLocation.label}
              </Text>
            </ModalSelector>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }
}

const styles = {
  selectDate: {
    width: (Dimensions.get("window").width - 48) / 2,
    textAlign: "center",
    color: "white",
    fontFamily: "Roboto-Regular",
    opacity: 0.8,
    marginBottom: 4
  },
  date: {
    backgroundColor: "white",
    width: (Dimensions.get("window").width - 48) / 2,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    shadowRadius: 14,
    shadowOpacity: 1,
    shadowOffset: { width: 4, height: 4 },
    shadowColor: "rgba(0.14, 0.14, 0.17, 0.07)",
    borderRadius: 5,
    elevation: 2
  },
  dropdownContainerStyle: {
    width: (Dimensions.get("window").width - 48) / 2 - 20
  }
};

export { DateLocationView };
