import React from 'react';
import { View, ActivityIndicator, Image, Text } from 'react-native';

const BrandLogo = require('../../images/glimpse_brand_logo_footer.png');

const Spinner = props => {
  if (props.flag === 'login') {
    return (
      <View style={styles.spinnerStyle}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <View style={styles.spinnerStyle}>
      <Image style={{ alignSelf: 'center', marginBottom: 8 }} source={BrandLogo} />
      <ActivityIndicator style={{ marginBottom: 8 }} color='#25AAE1' size="small" />
      <Text style={styles.textStyle}>Calculating Metrics...</Text>
    </View>
  );
};

const styles = {
  spinnerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  }
};

export { Spinner };
