import React, { Component } from "react";
import ModalSelector from "react-native-modal-selector";
import { View, Dimensions, Text, TouchableWithoutFeedback } from "react-native";
import { dateSet, getLocationSet } from "../../config/Utility";
import { UserData } from "../../config/UserData";

let locationSet = [];
let locationPos = [];
class LocationView extends Component {
  constructor(props) {
    super(props);

    this.state = { selectedDate: dateSet[5], selectedLocation: "" };

    UserData.getUserLocations()
      .then(res => {
        locationSet = getLocationSet(res);
        locationSet = locationSet.slice(1);
        this.setState(
          {
            selectedLocation: locationSet[0]
          },
          () => {
            this.props.onLocationSet(this.state.selectedLocation.value);
          }
        );
      })
      .catch(message => alert(message));
  }

  /*   getDerivedStateFromProps (props) {
  
    locationPos = locationSet.filter(elements => {
      return elements.value === props.labelLoc
    });
   this.setState({
      selectedLocation: locationPos

    })
    } */

  render() {
    return (
      <View>
        <View style={{ backgroundColor: "#82838c", height: 40 }} />
        <View style={{ marginTop: -40 }}>
          <View style={{ flexDirection: "row" }}>
            {this.renderLocationHeader()}
          </View>
          <View style={{ flexDirection: "row" }}>
            {this.renderLocationDropDown()}
          </View>
        </View>
      </View>
    );
  }

  /**
   * This function renders the header for the location drop down
   */
  renderLocationHeader() {
    const showLocation =
      this.props.showLocation === undefined ? true : this.props.showLocation;

    if (showLocation) {
      return (
        <Text style={[styles.selectDate, { marginLeft: 8, marginRight: 16 }]}>
          Select Location
        </Text>
      );
    }
  }

  /**
   * This function render drop down for location
   */
  renderLocationDropDown() {
    const showLocation =
      this.props.showLocation === undefined ? true : this.props.showLocation;

    if (showLocation) {
      return (
        <TouchableWithoutFeedback
          onPress={() => this.locationSelector.open()}
          disabled={this.props.disabled}
        >
          <View style={[styles.date, { marginLeft: 8, marginRight: 16 }]}>
            <ModalSelector
              data={locationSet}
              ref={selector => {
                this.locationSelector = selector;
              }}
              initValue=""
              accessible
              disabled={this.props.disabled}
              scrollViewAccessibilityLabel={"Scrollable options"}
              cancelButtonAccessibilityLabel={"Cancel Button"}
              animationType="none"
              cancelText="Cancel"
              onChange={option => {
                this.setState(
                  {
                    selectedLocation: option
                  },
                  () => {
                    this.props.onLocationSet(this.state.selectedLocation.value);
                  }
                );
              }}
            >
              <Text
                style={{
                  margin: 10,
                  color: "#394263",
                  fontFamily: "Roboto-Bold"
                }}
              >
                {this.state.selectedLocation.label}
              </Text>
            </ModalSelector>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }
}

const styles = {
  selectDate: {
    width: (Dimensions.get("window").width - 48) / 2,
    textAlign: "center",
    color: "white",
    fontFamily: "Roboto-Regular",
    opacity: 0.8,
    marginBottom: 4
  },
  date: {
    backgroundColor: "white",
    width: (Dimensions.get("window").width - 48) / 2,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    shadowRadius: 14,
    shadowOpacity: 1,
    shadowOffset: { width: 4, height: 4 },
    shadowColor: "rgba(0.14, 0.14, 0.17, 0.07)",
    borderRadius: 5,
    elevation: 2
  },
  dropdownContainerStyle: {
    width: (Dimensions.get("window").width - 48) / 2 - 20
  }
};

export { LocationView };
