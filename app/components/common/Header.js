import React from 'react';
import { View, Text, Image } from 'react-native';

const BrandLogo = require('../../images/glimpse_brand_logo_header.png');

const Header = ({ headerTitle }) => {
  if (headerTitle === 'Home') {
    return (
      <View style={{ flex: 1 }}>
        <Image style={{ alignSelf: 'center' }} source={BrandLogo} />
      </View>
    );
  }
  return (
    <View style={{ flex: 1 }}>
      <Text style={styles.headerTitleStyle}>{headerTitle}</Text>
    </View>
  );
};

const styles = {
  headerTitleStyle: {
    fontFamily: 'Roboto-Regular',
    fontWeight: '500',
    color: 'white',
    fontSize: 20,
    textAlign: 'center'
  }
};

export { Header };
