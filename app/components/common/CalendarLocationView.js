import React, { Component } from 'react';
import ModalSelector from 'react-native-modal-selector';
import DatePicker from 'react-native-datepicker';
import { View, Dimensions, Text, TouchableWithoutFeedback } from 'react-native';
import { getFormattedDate } from '../../config/Utility';
import { UserData } from '../../config/UserData';

const { width } = Dimensions.get('window');
let index = 0;
let today = null;

let locationSet = [
  {
    key: index,
    value: 0,
    label: 'All Locations'
  }
];

class CalendarLocationView extends Component {
  constructor(props) {
    super();
    let initialdate = new Date();
    today = getFormattedDate(initialdate);

    if (props.initialDateYesterday) {
      const todayTimeStamp = initialdate; // Unix timestamp in milliseconds
      const oneDayTimeStamp = 1000 * 60 * 60 * 24; // Milliseconds in a day
      const diff = todayTimeStamp - oneDayTimeStamp;
      initialdate = new Date(diff);
    }
    this.state = { selectedDate: getFormattedDate(initialdate), selectedLocation: '' };

    UserData.getUserLocations()
    .then(res => {
      const locations = JSON.parse(res);
      locationSet = [
        {
          key: index,
          value: 0,
          label: 'All Locations'
        }
      ];
      locations.forEach(value => {
        const location = {
          key: ++index,
          value: value.id,
          label: value.name
        };
        locationSet.push(location);
      });

      this.setState(
        {
          selectedLocation: locationSet[0]
        },
        () => {
          this.props.onDateOrLocationSet(this.state.selectedDate, this.state.selectedLocation.value);
        }
      );
    })
    .catch(message => alert(message));
  }

  render() {
    return (
      <View>
        <View style={{ backgroundColor: '#82838c', height: 40 }} />
        <View style={{ marginTop: -40 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            {this.renderDateHeader()}
            {this.renderLocationHeader()}
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            {this.renderDateDropDown()}

            {this.renderLocationDropDown()}
          </View>
        </View>
      </View>
    );
  }

  /**
  * This function renders the headers for the date drop down
  */
  renderDateHeader() {
    const showDate = this.props.showDate === undefined ? true : this.props.showDate;

    if (showDate) {
      return <Text style={[styles.selectDate, { marginLeft: 16, marginRight: 8 }]}>Select Date</Text>;
    }
  }

  /**
  * This function renders the headers for the location drop down
  */
  renderLocationHeader() {
    const showLocation = this.props.showLocation === undefined ? true : this.props.showLocation;

    if (showLocation) {
      return <Text style={[styles.selectDate, { marginLeft: 8, marginRight: 16 }]}>Select Location</Text>;
    }
  }

  /**
  * This function render drop down for date
  */
  renderDateDropDown() {
    const showDate = this.props.showDate === undefined ? true : this.props.showDate;

    if (showDate) {
      return (
        <DatePicker
          style={[styles.date, { marginLeft: 16, marginRight: 8 }]}
          date={this.state.selectedDate}
          mode="date"
          placeholder="select date"
          format="YYYY-MM-DD"
          maxDate={today}
          disabled={this.props.disabled}
          confirmBtnText="Done"
          cancelBtnText="Cancel"
          showIcon={false}
          customStyles={{
            dateInput: {
              borderWidth: 0
            },
            disabled: {
              backgroundColor: 'white'
            },
            dateTouchBody: {
              backgroundColor: 'white'
            },
            dateText: {
              margin: 10,
              color: '#394263',
              fontFamily: 'Roboto-Bold'
            }
          }}
          onDateChange={date => {
            this.setState(
              {
                selectedDate: date
              },
              () => {
                this.props.onDateOrLocationSet(this.state.selectedDate, this.state.selectedLocation.value);
              }
            );
          }}
        />
      );
    }
  }

  /**
  * This function renders drop down for location
  */
  renderLocationDropDown() {
    const showLocation = this.props.showLocation === undefined ? true : this.props.showLocation;

    if (showLocation) {
      return (
        <TouchableWithoutFeedback onPress={() => this.selector.open()} disabled={this.props.disabled}>
          <View style={[styles.date, { marginLeft: 8, marginRight: 16 }]}>
            <ModalSelector
              data={locationSet}
              ref={selector => { this.selector = selector; }}
              initValue="Select a location"
              accessible
              disabled={this.props.disabled}
              scrollViewAccessibilityLabel={'Scrollable options'}
              cancelButtonAccessibilityLabel={'Cancel Button'}
              animationType="none"
              cancelText="Cancel"
              onChange={option => {
                this.setState(
                  {
                    selectedLocation: option
                  },
                  () => {
                    this.props.onDateOrLocationSet(this.state.selectedDate, this.state.selectedLocation.value);
                  }
                );
              }}
            >
                <Text style={{ margin: 10, color: '#394263', fontFamily: 'Roboto-Bold' }}>{this.state.selectedLocation.label}</Text>
              </ModalSelector>
            </View>
          </TouchableWithoutFeedback>

        );
      }
    }
  }

  const styles = {
    selectDate: {
      width: (Dimensions.get('window').width - 48) / 2,
      textAlign: 'center',
      color: 'white',
      fontFamily: 'Roboto-Regular',
      opacity: 0.8
    },
    date: {
      backgroundColor: 'white',
      width: (width - 48) / 2,
      justifyContent: 'center',
      alignItems: 'center',
      height: 50,
      shadowRadius: 14,
      shadowOpacity: 1,
      shadowOffset: { width: 4, height: 4 },
      shadowColor: 'rgba(0.14, 0.14, 0.17, 0.07)',
      borderRadius: 5,
      elevation: 2
    }
  };

  export { CalendarLocationView };
