import React from 'react';
import { View, Text } from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import { UserData } from '../config/UserData';

let index = 0;
let practiceSet = [];

const forwardImage = '';

class PracticeIdView extends React.Component {
  constructor() {
    super();

    this.state = { selectedPractice: '' };

    UserData.getUserPractices()
      .then(res => {
        if (res !== null) {
          const practices = JSON.parse(res);
          practiceSet = [];
          practices.forEach(value => {
            const practice = {
              key: index++,
              value: value.id,
              label: value.name,
              accountTypeId: value.accountTypeId
            };
            practiceSet.push(practice);
          });
          this.setState(
            {
              selectedPractice: practiceSet[0]
            },
            () => {
              UserData.storeUserSelectedPractice(this.state.selectedPractice);
            }
          );
        } else {
          practiceSet = [];
        }
      })
      .catch(message => alert(message));
  }

  render() {
    return (
      <ModalSelector
        data={practiceSet}
        initValue="Select a location"
        accessible
        disabled={this.props.disabled}
        scrollViewAccessibilityLabel={'Scrollable options'}
        cancelButtonAccessibilityLabel={'Cancel Button'}
        animationType="none"
        cancelText="Cancel"
        onChange={option => {
          this.setState(
            {
              selectedPractice: option
            },
            () => {
              UserData.storeUserSelectedPractice(this.state.selectedPractice);
              this.props.onPracticeChanged(this.state.selectedPractice);
            }
          );
        }}
      >
        <View style={{ marginHorizontal: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <Text numberOfLines={1} style={{ color: '#394263', fontFamily: 'Roboto-Bold' }}>
            {this.state.selectedPractice.label}
          </Text>
          <Text
            style={{
              fontFamily: 'icomoon',
              fontSize: 15,
              fontWeight: '300',
              color: '#25AAE1',
              transform: [{ rotate: '90deg' }]
            }}
          >
            {forwardImage}
          </Text>
        </View>
      </ModalSelector>
    );
  }
}
export default PracticeIdView;