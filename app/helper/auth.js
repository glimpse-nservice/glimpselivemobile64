import { AsyncStorage } from 'react-native';

export const LOGGED_IN = 'token';

export const onLogIn = () => AsyncStorage.setItem(LOGGED_IN, 'true');

export const onLogOut = () => AsyncStorage.removeItem(LOGGED_IN);

export const isLoggedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(LOGGED_IN)
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};
