import {
  createStackNavigator,
  createDrawerNavigator,
  createSwitchNavigator,
  createAppContainer
} from "react-navigation";
import { Dimensions } from "react-native";

import LogIn from "../screens/Login/Login";
import Home from "../screens/Home/Home";
import DrawerScreen from "../screens/Drawer/Drawer";
import ContactUs from "../screens/ContactUs/ContactUs";
import KPI from "../screens/KPI/KPI";
import KPIDetail from "../screens/KPI/KPIDetail";
import DailySnapShot from "../screens/DailySnapShot/DailySnapshot";
import Opportunities from "../screens/Opportunities/Opportunities";
import Swot from "../screens/Swot/Swot";
import Notifications from "../screens/Notifications/Notifications";
import Refer from "../screens/ReferandEarn/Refer";
import SubmitSupportTicket from "../screens/SubmitSupportTicket/SubmitSupportTicket";
import NewUserForm from "../screens/AccountUpdate/NewUserForm";
import CustomerSettings from "../screens/CustomerSettings/CustomerSettings";
import UpdatePasswordForm from "../screens/AccountUpdate/UpdatePasswordForm";
import ForgetPasswordForm from "../screens/AccountUpdate/ForgetPasswordOkta";

export const LoggedOut = createStackNavigator({
  SignIn: {
    screen: LogIn
  }
});

const LoggedIn = createStackNavigator({
  HomeScene: {
    screen: Home
  },
  ContactUs: {
    screen: ContactUs
  },
  Notifications: {
    screen: Notifications
  },
  KPI: {
    screen: KPI
  },
  KPIDetail: {
    screen: KPIDetail
  },
  DailySnapShot: {
    screen: DailySnapShot
  },
  Opportunities: {
    screen: Opportunities,
    navigationOptions: {
      gesturesEnabled: false
    }
  },
  Swot: {
    screen: Swot
  },
  Refer: {
    screen: Refer
  },
  SupportTicket: {
    screen: SubmitSupportTicket
  },
  CustomerSettings: {
    screen: CustomerSettings
  },
  NewUserForm: {
    screen: NewUserForm
  },
  UpdatePasswordForm: {
    screen: UpdatePasswordForm
  },

  ForgetPasswordForm: {
    screen: ForgetPasswordForm
  }
});

export const Drawer = createDrawerNavigator(
  {
    Home: {
      screen: LoggedIn
    }
  },
  {
    contentComponent: DrawerScreen,
    drawerWidth: Dimensions.get("window").width * 0.7,
    drawerLockMode: "locked-closed",
    drawerPosition: "right"
  }
);

export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      LoggedIn: {
        screen: Drawer
      },
      LoggedOut: {
        screen: LoggedOut
      }
    },
    {
      initialRouteName: signedIn ? "LoggedIn" : "LoggedOut"
    }
  );
};
