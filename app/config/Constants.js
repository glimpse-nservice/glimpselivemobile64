export default {
  //Navigation Bar Style Values
  headerStyle: {
    backgroundColor: '#82838c',
    elevation: 0,
    borderBottomWidth: 0
  },
  headerTintColor: '#fff',
  sideMenuStyle: {
    fontSize: 20,
    paddingHorizontal: 16,
    fontFamily: 'icomoon',
    color: 'white'
  },
  appColor: '#25AAE1',
  urlForgotPwd: 'https://www.glimpselive.com/login.html',
  urlFullReport: 'https://www.glimpselive.com',
  urlHelp: 'https://www.glimpselive.com/common/help-center.php',
  urlTerms: 'https://www.glimpselive.com/terms.html',
  referEmailSubject: 'Referral',
  salesEmail: 'sales@glimpselive.com',
  supportEmail: 'support@glimpselive.com'
};
