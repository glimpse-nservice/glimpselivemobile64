import { AsyncStorage } from 'react-native';

export const USER_LOCATIONS = 'locations';
export const USER_SELECTED_PRACTICE = 'selected_practice';
export const USER_LOGIN_ID = 'login_id';
export const USER_NAME = 'username';
export const USER_PRACTICE = 'practices';

export const UserData = {
  storeUserLocations: locations => {
    AsyncStorage.setItem(USER_LOCATIONS, JSON.stringify(locations));
  },

  storeUserSelectedPractice: practice => {
    AsyncStorage.setItem(USER_SELECTED_PRACTICE, JSON.stringify(practice));
  },

  storeUserLoginId: loginId => {
    AsyncStorage.setItem(USER_LOGIN_ID, loginId.toString());
  },

  storeUserName: username => {
    AsyncStorage.setItem(USER_NAME, username);
  },

  storeUserPractices: practices => {
    AsyncStorage.removeItem(USER_PRACTICE).then(
      () => {
        //console.log('resolved');
        AsyncStorage.setItem(USER_PRACTICE, JSON.stringify(practices));
      },
      () => {
       //console.log('rejected');
      }
    );
  },

  getUserLocations: () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(USER_LOCATIONS)
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch(err => reject(err));
    });
  },

  getUserSelectedPractice: () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(USER_SELECTED_PRACTICE)
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch(err => reject(err));
    });
  },

  getUserLoginId: () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(USER_LOGIN_ID)
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch(err => reject(err));
    });
  },

  getUserName: () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(USER_NAME)
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch(err => reject(err));
    });
  },

  getUserPractices: () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(USER_PRACTICE)
        .then(res => {
          //console.log(res);

          if (res !== null) {
            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch(err => reject(err));
    });
  },

  clearAll: () => {
    return new Promise((resolve, reject) => {
      AsyncStorage.clear()
        .then(res => {
          if (res !== null) {
            resolve(res);
          } else {
            resolve(null);
          }
        })
        .catch(err => reject(err));
    });
  }
};
