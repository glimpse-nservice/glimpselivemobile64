import Constants from "./Constants.js";

export const Strings = {
  login: {
    strSignInTag: "Sign in to ABB Analyze",
    strEnterDetailsTag: "Enter your details below.",
    strUserName: "Username",
    strPassword: "Password",
    strForgotPwd: "Forgot Password?",
    strEnterYourUsername: "Enter your Username",
    strEnterYourPwd: "Enter your password",
    strSignIn: "Sign In",
    strLoginError: "Please enter correct credentials",
    strUsernameError: "Please enter username",
    strPwdError: "Please enter password"
  },
  drawer: {
    strGlimpse: "ABB Analyze",
    strKPIsList: "KPI's List",
    strKpiFavorites: "KPI Favorites",
    strSubmitSupportTicket: "Submit Support Ticket",
    strDailySnapshot: "Daily Snapshot",
    strOpportunities: "Opportunities",
    strSwot: "SWOT",
    strContactUs: "Contact Us",
    strSignOut: "Sign Out"
  },
  home: {
    strHome: "Home",
    strKpiFavorites: "KPI FAVORITES",
    strDailySnapshot: "DAILY SNAPSHOT",
    strOpportunities: "OPPORTUNITIES",
    strKPIsList: "KPI's LIST",
    strSwot: "SWOT",
    strHelp: "HELP",
    strSeeFullReport: "SEE FULL REPORT",
    strAlert: "ALERT",
    strAlertText: "You will be moved out of the app.",
    strOk: "OK",
    strCancel: "CANCEL"
  },
  contactUs: {
    strContactUs: "Contact Us",
    strCustomerService: "Customer Service",
    strEmailSupport: "Email Support",
    strSalesSupport: "Sales",
    strSupportEmail: Constants.supportEmail,
    strSalesEmail: Constants.salesEmail,
    strPhoneNo: "904-503-9616",
    strSupportNo: "1-888-413-5245"
  },
  notifications: {
    strNotifications: "Notifications",
    strSwotReport: "Swot Report",
    strMonthlyMissedOpportunities: "Monthly Missed Opportunities",
    strEngagement: "Engagement"
  },
  kpi: {
    strKPIList: "KPI List",
    strKPIFavorites: "KPI Favorites"
  },

  kpiDetail: {},
  refer: {
    strReferTitle: "Refer a Friend and earn \n 2 months free when they join",
    strReferDesc:
      "When a friend signs up for ABB Analyze, you will receive \n two months free and your friend will also receive \n two months free"
  },
  swot: {
    strSwot: "SWOT"
  },
  dailySnapshot: {
    strDailySnapshot: "Daily SnapShot"
  },
  errorMsgs: {
    strErrBrowser: "No Browser found!!"
  },
  submitSupportTicket: {
    strEmail: "Email",
    strEnterEmail: "Enter your Email",
    strSubject: "Subject",
    strPractice: "Practice",
    strTicketInfo: "Ticket Information",
    strSendEmail: "Send Email",
    strEmailError: "Please enter Email",
    strTicketError: "Ticket info cannot be empty"
  },
  referForm: {
    strTitle: "Referral Info",
    strRefer: "Refer",
    strEnterReferralEmail: "Enter Referral email",
    strEnterReferralName: "Enter Referral name",
    strReferralNameError: "Referral name cannot be empty",
    strReferralName: "name"
  },
  updateUsers: {
    strTitle: "Complete Account Setup",
    strForgetPassword: "Forgot Password",
    strUserIntro: "Enter your User Name to reset your password.",
    strPasswordTag: "Create Your New Password",
    strQuestionTitle: "Setup Your Security Questions and Answers",
    strAnswer: "Enter Answer",
    strVerifyInstruct: "Please answer the following security questions:",
    strVerifyTitle: "Verify Your Account"
  }
};
