/*
* This function gets the date in
* yyyy/mm/dd format
* @param {date to be formatted} date
*/

import moment from 'moment';
import { Dimensions, Platform, Linking, Alert } from 'react-native';
import { CustomTabs } from 'react-native-custom-tabs';
import SafariView from 'react-native-safari-view';
import { Strings } from '../config/Strings';


let dateIndex = 0;

export const dateSet = [
  {
    key: dateIndex++,
    value: 31,
    label: 'Last 30 days'
  },
  {
    key: dateIndex++,
    value: 32,
    label: 'Month to Date'
  },
  {
    key: dateIndex++,
    value: 33,
    label: 'Last Month'
  },
  {
    key: dateIndex++,
    value: 34,
    label: 'Quater to Date'
  },
  {
    key: dateIndex++,
    value: 35,
    label: 'Last Quater'
  },
  {
    key: dateIndex++,
    value: 36,
    label: 'Year to Date'
  },
  {
    key: dateIndex++,
    value: 37,
    label: 'Last Year'
  }
];

export function getLocationSet(res) {
  let index = 0;

  const locationSet = [];

  const locations = JSON.parse(res);
  locationSet.push({
    key: index,
    value: 0,
    label: 'All Locations'
  });

  locations.forEach(value => {
    const location = {
      key: ++index,
      value: value.id,
      label: value.name
    };
    locationSet.push(location);
  });

  return locationSet;
}

export function getStartAndEndDate(value) {
  let startDate = '';
  let endDate = '';

  const todayMoment = moment().subtract(1, 'days');
  const dateFormat = 'YYYY-MM-DD';
  const todayDate = todayMoment.format(dateFormat);

  switch (value) {
    /*Last 30 days*/
    case dateSet[0].value:
      startDate = todayMoment.subtract(29, 'days').format(dateFormat);
      endDate = todayDate;
      break;

    /*Month to Date*/
    case dateSet[1].value:
      startDate = todayMoment.startOf('month').format(dateFormat);
      endDate = todayDate;
      break;

    /*Last Month*/
    case dateSet[2].value:
      {
        const prevMonth = todayMoment.subtract(1, 'months');
        startDate = prevMonth.startOf('month').format(dateFormat);
        endDate = prevMonth.endOf('month').format(dateFormat);
      }
      break;

    /*Quarter to Date*/
    case dateSet[3].value:
      startDate = todayMoment.startOf('quarter').format(dateFormat);
      endDate = todayDate;
      break;

    /*Last Quarter*/
    case dateSet[4].value:
      {
        const prevQuarter = todayMoment.subtract(1, 'quarter');
        startDate = prevQuarter.startOf('quarter').format(dateFormat);
        endDate = prevQuarter.endOf('quarter').format(dateFormat);
      }
      break;

    /*Year to Date*/
    case dateSet[5].value:
      startDate = todayMoment.startOf('year').format(dateFormat);
      endDate = todayDate;
      break;

    /*Last Year*/
    case dateSet[6].value:
      {
        const prevYear = todayMoment.subtract(1, 'year');
        startDate = prevYear.startOf('year').format(dateFormat);
        endDate = prevYear.endOf('year').format(dateFormat);
      }
      break;
    default:

  }

  return { startDate, endDate };
}

export const getFormattedDate = date => {
  if (date !== null && date !== '') {
    let today = date;
    let dd = today.getDate();
    let mm = today.getMonth() + 1;

    const yyyy = today.getFullYear();
    if (dd < 10) {
      dd = `0${dd}`;
    }
    if (mm < 10) {
      mm = `0${mm}`;
    }
    today = `${yyyy}-${mm}-${dd}`;

    return today;
  }
  return;
};

export const numberWithCommas = x => {
  var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
};

export  const formatNumber = number => {
  if (number < 1000){
    return number.toFixed(2);
  }
  else 
  return number.toFixed();
}

export const formatType = (type, number) => {
  switch (type) {
    case 'currency':
    return `$${numberWithCommas(formatNumber(number))}`
      break;
    case 'pct':
    return `${number.toFixed(1)}%`;
      break;
    case 'units':
 return `${numberWithCommas(number)} units`;
      break;
    default:
  }

}
/**
 * Returns true if the screen is in portrait mode
 */
export const isPortrait = () => {
  const dim = Dimensions.get('screen');
  return dim.height >= dim.width;
};

/**
 * This function opens url in google tabs if
 * platform is android else open in safari view
 * @param {url to open} urlToOpen
 */
export const openURL = urlToOpen => {
  if (Platform.OS === 'ios') {
    SafariView.show({
      url: urlToOpen
    });
  } else {
    CustomTabs.openURL(urlToOpen).catch(() => {
      Linking.openURL(urlToOpen).catch(() => {
        Alert.alert(Strings.errorMsgs.strErrBrowser);
      });
    });
  }
};

