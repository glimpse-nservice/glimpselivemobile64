import SplashScreen from "react-native-splash-screen";
import React, { Component } from "react";
import { createAppContainer } from "react-navigation";

import { YellowBox, Platform } from "react-native";
import { createRootNavigator } from "./router";
import { isLoggedIn } from "./helper/auth";
import OneSignal from "react-native-onesignal";

YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);

export default class App extends Component {
  constructor(props) {
    super(props);
    OneSignal.init("87a2b605-2679-4de7-864c-7e80ea6584de");
    this.state = {
      loggedIn: false,
      checkedLogIn: false
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    isLoggedIn()
      .then(res => this.setState({ loggedIn: res, checkedLogIn: true }))
      .catch(() => alert("An error occurred"));
  }

  render() {
    const { checkedLogIn, loggedIn } = this.state;
    const RootNavigator = createRootNavigator(loggedIn);
    const AppContainer = createAppContainer(RootNavigator);
    // If we haven't checked AsyncStorage yet, don't render anything
    if (!checkedLogIn) {
      return null;
    }
    return <AppContainer />;
  }
}
