import Reactotron from "reactotron-react-native";

Reactotron.configure({
  //host: "192.168.1.122", //home
  //host: "10.22.120.35",
  //host: "192.168.43.72", // hotspot
  //host: "192.168.1.122",
  host: "10.22.120.11",
  enabled: true,
  port: 9090
}) // controls connection & communication settings
  .useReactNative() // add all built-in react native plugins
  .connect(); // let's connect!
