import axios from "axios";

export default axios.create({
  // baseURL: "http://api-dev.glimpselive.com/api/",
  baseURL: "https://api.glimpselive.com/api/",
  // timeout: 30000,
  // baseURL: "http://api-dev.glimpselive.com/api/",
  //baseURL: "http://10.22.120.35:14406/api/", // office local dev
  //baseURL: "http://192.168.1.122:14406/api/", //home
  crossdomain: true,
  headers: {
    Authorization: "IvankaMobileSide"
    // Authorization: "IvnakaFrontSide"
  }
});

// ----------------------APIs----------------------------------
//Login Append User name and password
export const URL_LOGIN = "auth/login/";
//Daily snapshot
export const URL_DAILY_SNAPSHOTS = (practiceId, locationId, date) => {
  return `metrics/${practiceId}/${locationId}/0/g-snapshotgross/0/${date}/${date}/0/0/1`;
};

//KPI Favorites List
export const URL_KPI_FAV = (
  practiceId,
  locationId,
  loginId,
  startDate,
  endDate
) => {
  return `metrics/kpifavorites/${practiceId}/${locationId}/${loginId}/${startDate}/${endDate}`;
};

export const URL_KPI_LIST = (
  practiceId,
  locationId,
  loginId,
  metricName,
  startDate,
  endDate
) => {
  return `metrics/kpiList/${metricName}/${practiceId}/${locationId}/${loginId}/${startDate}/${endDate}`;
};

//KPI Detail
export const URL_KPI_DETAIL_LIST = (
  practiceId,
  locationId,
  startDate,
  endDate,
  metricKey
) => {
  return `metrics/${practiceId}/${locationId}/0/m-${metricKey}/0/${startDate}/${endDate}/0/date/1`;
};
//Add/Remove to/from favorite
export const URL_ADD_REMOVE_FAVORITE = (loginId, metric, isFavorite) => {
  return `metrics/kpiSet/${loginId}/${metric}/${isFavorite ? "0" : "1"}`;
};
//Swot
export const URL_SWOT = (locationId, startDate, endDate) => {
  return `swot/${locationId}/${startDate}/${endDate}`;
};
//Opportunities
export const URL_OPPORTUNITIES = (
  category,
  practiceId,
  locationId,
  startDate,
  endDate
) => {
  return `opportunities/${category}/${practiceId}/${locationId}/${startDate}/${endDate}`;
};

//get security questions
export const URL_SECURITY_QUESTIONS = "auth/securityQuestionList";

//update user auth
export const URL_USER_SET_UP = (
  userId,
  userName,
  passsword,
  securityQuestionId1,
  answer1,
  securityQuestionId2,
  answer2
) => {
  return `auth/updateAuthorization/${userId}/${userName}/${passsword}/${securityQuestionId1}/${answer1}/${securityQuestionId2}/${answer2}/`;
};

//get user security question
export const URL_USER_SECURITY_QUESTIONS = userName => {
  return `auth/securityQuestion/${userName}/`;
};

//check user security answers
export const URL_USER_CHECK_SECURITY_ANSWERS = (userName, answer1, answer2) => {
  return `auth/securityQuestion/${userName}/${answer1}/${answer2}/`;
};

export const URL_UPDATE_PASSWORD = (userId, userName, password) => {
  return `auth/updateAuthorization/${userId}/${userName}/${password}/`;
};

export const URL_LOGGING = (practiceId, locationId, loginId, url) => {
  return `userManagement/trackUser/${practiceId}/${locationId}/${loginId}/?url=${url}`;
};

//get account manager info
export const URL_ACCOUNT_MANAGER_INFO = practiceId => {
  return `account/accountManager/${practiceId}`;
};
