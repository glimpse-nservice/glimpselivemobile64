import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Linking,
  ScrollView
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import { Avatar, ButtonGroup } from "react-native-elements";
import call from "react-native-phone-call";
import { Strings } from "../../config/Strings";
import styles from "./ContactUsStyle";
import Constants from "../../config/Constants";
import { Header } from "../../components/common/index";
import WebService, { URL_ACCOUNT_MANAGER_INFO } from "../../webservice";
import { UserData } from "../../config/UserData";

const {
  strSupportNo,
  strCustomerService,
  strSupportEmail,
  strPhoneNo,
  strSalesEmail
} = Strings.contactUs;

const iconSupport = <Icon name="wrench" size={20} />;
const supportEmailIcon = <Icon name="envelope" size={20} />;
const mainPhone = <Icon name="phone" size={20} />;
const iconAddress = <Icon name="address-card" size={20} />;

export default class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      selectedIndex: 2,
      accountManagerEmail: strSupportEmail,
      accountManagerPhone: strPhoneNo,
      accountManagerName: "Pending",
      accountManagerTitle: "Account Manager",
      practiceId: 0,
      phoneExt: 0
    };
    this.updateIndex = this.updateIndex.bind(this);
  }

  componentDidMount() {
    UserData.getUserSelectedPractice()
      .then(res => {
        const userPractice = JSON.parse(res);
        this.setState({
          practiceId: userPractice.value
        });

        WebService.get(URL_ACCOUNT_MANAGER_INFO(this.state.practiceId)).then(
          response => {
            const jsonData = response.data[0];
            if (jsonData.length !== 0) {
              this.setState(
                {
                  isLoading: false,
                  accountManagerEmail: jsonData.email,
                  accountManagerPhone: `${jsonData.phone}`,
                  phoneExt: jsonData.phoneEXt,
                  accountManagerName: `${jsonData.firstName}  ${
                    jsonData.lastName
                  }`
                },
                () => {}
              );
            }
          }
        );
      })
      .catch(() => {});
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex });
    if (selectedIndex === 0) {
      if (this.state.phoneExt === 0) {
        Linking.openURL(`tel:${strSupportNo}`);
      } else {
        const args = {
          number: `${this.state.accountManagerPhone},,, ${this.state.phoneExt}`, // Use commas to add time between digits.``
          prompt: false
        };

        call(args).catch(console.error);
      }
    } else if (selectedIndex === 1) {
      Linking.openURL(`mailto:${this.state.accountManagerEmail}`);
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle={strCustomerService} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  render() {
    const iconEmail = () => (
      <View style={{ alignItems: "center" }}>
        <Icon name="envelope" size={35} color={Constants.appColor} />
        <Text style={{ fontSize: 12 }}>{this.state.accountManagerEmail}</Text>
      </View>
    );

    const iconPhone = () => (
      <View style={{ alignItems: "center" }}>
        <Icon
          name="phone"
          size={35}
          color={Constants.appColor}
          iconStyle={{ alignSelf: "center" }}
        />
        <Text style={{ fontSize: 12 }}>{`${
          this.state.accountManagerPhone
        } ext ${this.state.phoneExt}`}</Text>
      </View>
    );
    const buttons = [{ element: iconPhone }, { element: iconEmail }];
    const { selectedIndex } = this.state;
    const {
      iconCustomerServiceStyle,
      fieldContainer,
      fieldIcon,
      fieldText,
      accountManagerStyle,
      headerStyles,
      addressFieldText
    } = styles;

    return (
      <ScrollView style={{ backgroundColor: "#fff" }}>
        <View
          style={{
            alignItems: "center",
            paddingBottom: 10,
            borderBottomColor: "#394263",
            borderBottomWidth: 1
          }}
        >
          <Avatar
            size={110}
            rounded
            icon={{
              name: "user-circle",
              type: "font-awesome",
              size: 100
            }}
            containerStyle={iconCustomerServiceStyle}
          />

          <Text style={accountManagerStyle}>
            {this.state.accountManagerName}
          </Text>
          <Text>{this.state.accountManagerTitle}</Text>

          <ButtonGroup
            onPress={this.updateIndex}
            buttons={buttons}
            selectedIndex={selectedIndex}
            containerStyle={{
              backgroundColor: "transparent",
              marginTop: 20,
              borderWidth: 0,
              height: 60
            }}
            selectedButtonStyle={{
              backgroundColor: "transparent"
            }}
          />
        </View>

        <View style={{ alignItems: "center", marginTop: 50 }}>
          {/* Phone number */}

          <Text style={headerStyles}>PHONE NUMBERS </Text>

          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:${strPhoneNo}`)}
              style={{ width: "50%" }}
            >
              <View style={fieldContainer}>
                <Text style={fieldIcon}>{mainPhone}</Text>
                <Text style={fieldText}>{strPhoneNo}</Text>
              </View>
            </TouchableOpacity>
            {/* support */}
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:${strSupportNo}`)}
              style={{ width: "50%" }}
            >
              <View style={fieldContainer}>
                <Text style={fieldIcon}>{iconSupport}</Text>
                <Text style={fieldText}>{strSupportNo}</Text>
              </View>
            </TouchableOpacity>
          </View>

          <Text style={headerStyles}>EMAIL </Text>

          <TouchableOpacity
            onPress={() => Linking.openURL(`mailto:${strSalesEmail}`)}
          >
            <View style={fieldContainer}>
              <Text style={fieldIcon}>{supportEmailIcon}</Text>
              <Text style={fieldText}>{strSupportEmail}</Text>
            </View>
          </TouchableOpacity>
          <Text style={headerStyles}>ADDRESS </Text>

          <View style={fieldContainer}>
            <Text style={fieldIcon}>{iconAddress}</Text>
            <Text style={addressFieldText}>
              {
                "1909 Beach Boulevard,  Suite 202 \n Jacksonville Beach, FL 32250"
              }
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}
