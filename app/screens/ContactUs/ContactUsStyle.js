import Constants from '../../config/Constants';

export default {


  iconCustomerServiceStyle: {
    marginTop: 30,
    marginBottom: 10
  },
  textCustomerService: {
    fontSize: 30,
    fontFamily: 'Roboto-Regular',
    color: '#394263',
    fontWeight: '500',
    marginLeft: 24,
    marginBottom: 24
  },

  accountManagerStyle: {
    fontSize: 30,
    fontFamily: 'Roboto-Regular',
    color: '#394263',
    fontWeight: '500'
  },
  fieldContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 24,
    marginRight: 24,
   
  },
  fieldIcon: {
    color: Constants.appColor
  },
  fieldText: {
    marginLeft: 10,
    fontSize: 16,
    fontFamily: 'Roboto-Regular',
    color: '#394263',
    fontWeight: 'bold',
  
  },

  addressFieldText: {
    marginLeft: 10,
    fontSize: 14,
    fontFamily: 'Roboto-Regular',
    color: '#394263',
   
  },

  emailSupport: {

    color: Constants.appColor,
    marginLeft: 22,

    fontFamily: 'Roboto-Regular',
    marginRight: 32
  },
  email: {
     marginLeft: 22,
    marginRight: 32,
    fontFamily: 'Roboto-Regular',
    color: '#394263',
    fontWeight: 'bold',
  },

  headerStyles: {
    margin: 10
  }
};
