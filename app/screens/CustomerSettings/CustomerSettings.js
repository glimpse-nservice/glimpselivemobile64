import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { Header } from '../../components/common';
import Constants from '../../config/Constants';

export default class CustomerSettings extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle='Customer Settings' />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Custom Notifications Coming Soon</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
});
