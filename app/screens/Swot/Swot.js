import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ScrollView,
  TouchableOpacity
} from "react-native";
import Snackbar from "react-native-snackbar";
import Constants from "../../config/Constants";
import {
  DateLocationView,
  Spinner,
  Header
} from "../../components/common/index";
import styles from "./SwotStyles";
import WebService, { URL_SWOT } from "../../webservice";
import { Strings } from "../../config/Strings";
import {
  getStartAndEndDate,
  dateSet,
  formatType,
  numberWithCommas
} from "../../config/Utility";
import demoData from "../../data/SwotData";
import { UserData } from "../../config/UserData";

let isMounted = false;

export default class Swot extends Component {
  constructor() {
    super();

    this.state = {
      data: null,
      loading: false,
      startDate: "",
      location: "0",
      date: dateSet[0],
      disableSelector: false
    };
  }

  componentDidMount() {
    isMounted = true;
  }

  componentWillUnmount() {
    isMounted = false;
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle={Strings.swot.strSwot} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  /**
   * This function is called whenever user selects a date
   * @param { new date that is selected by the user } dateObj
   */
  onDateOrLocationSet(dateObj, newLocation) {
    const date = getStartAndEndDate(dateObj.value);

    if (dateObj !== this.state.date || newLocation !== this.state.location) {
      this.setState(
        {
          date: dateObj,
          location: newLocation
        },
        () => {
          this.setState({ loading: true, disableSelector: true });
          this.getSwotDataFromServer(
            this.state.location,
            date.startDate,
            date.endDate
          );
        }
      );
    }
  }

  /**
   * This function fetch data from server
   * according to the location and date set by
   * the user
   * @param {*} locationId
   * @param {*} startDate
   * @param {*} endDate
   */
  getSwotDataFromServer(locationId, startDate, endDate) {
    this.setState({ loading: true, disableSelector: true });

    if (locationId === 0) {
      message = "SWOT Analysis is not available on the practice level";

      Snackbar.show({
        title: message,
        duration: Snackbar.LENGTH_LONG
      });
      this.setState({ loading: false, disableSelector: false });
    } else {
      WebService.get(URL_SWOT(locationId, startDate, endDate))
        .then(response => {
          if (isMounted) {
            this.setState({
              data: response.data[0],
              loading: false,
              disableSelector: false
            });
          }
        })
        .catch(error => {
          let message = error.message;
          Snackbar.show({
            title: message,
            duration: Snackbar.LENGTH_LONG
          });
          this.setState({ loading: false, disableSelector: false });
        });
    }
  }

  /**
   * This function renders metrics on the screen
   * @param {metric data} metric
   */
  renderMetrics(metric) {
    const item = metric.item;
    var type = item.metricSymbol;
    var number = item.metricData;
    const data = formatType(type, number);

    const headerText = `${item.metricName}: ${data} (${item.metricRanking})`;
    return (
      <View style={{ padding: 10 }} key={item.metricsTemplateId}>
        <Text style={styles.metricHeaderStyle}>{headerText}</Text>
        <Text style={styles.metricDescStyle}>{item.metricContent}</Text>
      </View>
    );
  }

  /**
   * This function renders the data according to
   * the data passed.
   * @param {name of the header} headerName
   * @param {metric data} data
   */
  renderDataOnScreen(headerName, data) {
    let colorForHeader = "";
    switch (headerName) {
      case "Strengths":
        colorForHeader = "#915FAB";
        break;

      case "Weaknesses":
        colorForHeader = "#ffa500";
        break;

      case "Opportunities":
        colorForHeader = "#008000";
        break;

      case "Threats":
        colorForHeader = "#800000";
        break;

      default:
        colorForHeader = "#915FAB";
    }
    if (data !== null && data.length > 0) {
      return (
        <View key={headerName}>
          {/*Header*/}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 20,
              paddingLeft: 10
            }}
          >
            <Text style={[styles.headerIconStyle, { color: colorForHeader }]}>
              
            </Text>

            <Text style={[styles.headerTextStyle, { color: colorForHeader }]}>
              {headerName}
            </Text>
          </View>

          {/*Metrics*/}
          <FlatList
            data={data}
            keyExtractor={metric => metric.metricsTemplateId.toString()}
            renderItem={metric => this.renderMetrics(metric)}
          />
        </View>
      );
    }
  }

  /**
   * This function renders the main view i.e.
   * strengths, threats, weakness, and opportunities
   */
  renderMainView() {
    if (this.state.loading) {
      return <Spinner />;
    }
    if (this.state.data !== null) {
      return (
        <ScrollView keyboardShouldPersistTaps="never">
          <View style={{ margin: 15 }}>
            {this.renderDataOnScreen("Strengths", this.state.data.strengths)}
            {this.renderDataOnScreen("Weaknesses", this.state.data.weaknesses)}
            {this.renderDataOnScreen(
              "Opportunities",
              this.state.data.opportunities
            )}
            {this.renderDataOnScreen("Threats", this.state.data.threats)}
          </View>
        </ScrollView>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <DateLocationView
          disabled={this.state.disableSelector}
          onDateOrLocationSet={this.onDateOrLocationSet.bind(this)}
          labelLoc={this.state.location}
          labelDate={this.state.date}
          flag="Swot"
        />
        {this.renderMainView()}
      </View>
    );
  }
}
