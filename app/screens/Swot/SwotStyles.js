export default {
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  headerIconStyle: {
    fontSize: 24,
    fontFamily: 'icomoon'
  },
  headerTextStyle: {
    fontSize: 20,
    marginLeft: 10,
    fontFamily: 'Roboto-Bold'
  },
  metricHeaderStyle: {
    fontSize: 16,
    color: 'black',
    margin: 2,
    fontFamily: 'Roboto-Bold'
  },
  metricDescStyle: {
    fontSize: 14,
    color: '#c3c3c3',
    margin: 2,
    fontFamily: 'Roboto-Regular'
  }
};
