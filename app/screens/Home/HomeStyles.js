import { Dimensions } from "react-native";
import Constants from "../../config/Constants";

export default {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  gridHeader: {
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 25,
    padding: 10,
    marginTop: 20,
    marginBottom: 10,
    color: "black",
    fontFamily: "Roboto-Medium"
  },
  gridView: {
    flex: 1,
    backgroundColor: "#ffffff",
    marginTop: 40,
    alignSelf: "center"
  },
  itemContainer: {
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 7,
    borderColor: "#E5E5E5",
    borderTopWidth: 1,
    elevation: 3,
    padding: 10,
    height: Dimensions.get("window").height / 5 - 20,
    shadowRadius: 14,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "rgba(0, 0, 0, 0.18)"
  },
  itemImage: {
    fontSize: 34,
    fontFamily: "icomoon",
    color: Constants.appColor
  },
  itemName: {
    fontSize: 12,
    padding: 10,
    color: "#394263",
    fontFamily: "Roboto-Regular"
  },
  itemNameDisabled: {
    fontSize: 12,
    padding: 10,
    color: "#999",
    fontFamily: "Roboto-Regular"
  },
  itemImageDisabled: {
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 7,
    borderColor: "#E5E5E5",
    borderTopWidth: 1,
    elevation: 3,
    padding: 10,
    height: Dimensions.get("window").height / 5 - 20,
    shadowRadius: 14,
    opacity: 0.5,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "rgba(0, 0, 0, 0.18)"
  },
  fullReportView: {
    marginTop: 16,
    marginLeft: 24,
    marginRight: 24,
    backgroundColor: Constants.appColor,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    borderRadius: 25
  },
  fullReportIcon: {
    fontSize: 24,
    fontFamily: "icomoon",
    color: "white"
  },
  fullReportText: {
    marginLeft: 16,
    color: "white",
    fontFamily: "Roboto-Regular",
    fontWeight: "500"
  }
};
