import React from "react";
import FlatGrid from "react-native-super-grid";

import { View, Text, TouchableOpacity, TouchableWithoutFeedback, Dimensions, ScrollView } from "react-native";
import { withNavigation } from "react-navigation";
import { Strings } from "../../config/Strings";
import styles from "./HomeStyles";
import Constants from "../../config/Constants";
import { Header } from "../../components/common";
import PracticeIdView from "../../components/PracticeIdView";
import WebService, { URL_LOGGING } from "../../webservice/index";
import { UserData } from "../../config/UserData";
const { strHome, strKpiFavorites, strDailySnapshot, strOpportunities, strKPIsList, strSwot, strHelp } = Strings.home;
let index = 0;
let practiceSet = [];

global.IsSelectedLite = null;
class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loginId: 0,
      practiceId: 0,
      selectedPractice: UserData.selectedPractice,
      flag: this.handleDisableEnable()
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: <View />,
      headerRight: (<TouchableOpacity onPress={navigation.openDrawer}>
        <Text style={Constants.sideMenuStyle}></Text>
      </TouchableOpacity>
      ),
      headerTitle: <Header headerTitle={strHome} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerBackTitle: null
    };
  };

  componentDidMount() {
    WebService.interceptors.request.use(
      config => {
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );

  }

  handleDisableEnable() {
    // make sure the selected practice is empty before taking the first in array 
    this.state = { selectedPractice: '' };
    UserData.getUserPractices()
      .then(res => {
        if (res !== null) {
          const practices = JSON.parse(res);
          practiceSet = [];
          practices.forEach(value => {
            const practice = {
              key: index++,
              value: value.id,
              label: value.name,
              accountTypeId: value.accountTypeId
            };
            practiceSet.push(practice);
          });
          this.setState(
            {
              selectedPractice: practiceSet[0]
            },
            () => {
              UserData.storeUserSelectedPractice(practiceSet[0]);
              console.log(practiceSet[0]);
              if (practiceSet[0] != null) {
                if (practiceSet[0].accountTypeId != undefined) {
                  if (practiceSet[0].accountTypeId == 2) {
                    this.setState({ flag: true });
                    global.IsSelectedLite = this.flag;
                  }

                  else {
                    this.setState({ flag: false });
                    global.IsSelectedLite = this.flag;
                  }
                }
              }
            }
          );
        } else {
          practiceSet = [];
        }
      })
      .catch(message => alert(message));
    return true;

  }
  homeItemTapped(itemName) {
    // get where user logged to and then  go to the page
    UserData.getUserLoginId()
      .then(loginId => {
        this.setState({ loginId });
        return UserData.getUserSelectedPractice();
      })
      .then(userPractice => {
        const practice = JSON.parse(userPractice);
        practiceId = practice.value;
        this.setState({ practiceId: practiceId });
        WebService.post(
          URL_LOGGING(
            this.state.practiceId,
            0,
            this.state.loginId,
            `mobile/${itemName}`
          )
        );
        switch (itemName) {
          case strKpiFavorites:
            this.props.navigation.navigate("KPI", {
              isFavorite: true
            });
            break;
          case strDailySnapshot:
            this.props.navigation.navigate("DailySnapShot");
            break;
          case strOpportunities:
            if (!global.IsSelectedLite) {
              this.props.navigation.navigate("Opportunities");
            }
            break;
          case strKPIsList:
            this.props.navigation.navigate("KPI", {
              isFavorite: false
            });
            break;
          case strSwot:
            if (!global.IsSelectedLite) { this.props.navigation.navigate("Swot"); }
            break;
          case strHelp:
            this.props.navigation.navigate("ContactUs");
            break;
          default:
            break;
        }
      })
      .catch(error => {
      });
  }
  styleitem(name) {
    // if ((name == "OPPORTUNITIES" || name == "SWOT") && this.state.flag) {
    if ((name == "OPPORTUNITIES" || name == "SWOT") && global.IsSelectedLite) {
      return styles.itemNameDisabled
    }
    else
      return styles.itemName
  }
  styleimage(name) {
    if ((name == "OPPORTUNITIES" || name == "SWOT") && global.IsSelectedLite) {
      return styles.itemImageDisabled;
    }
    else
      return styles.itemContainer;
  }

  render() {
    const items = [
      { name: strKpiFavorites, image: "" },
      { name: strDailySnapshot, image: "" },
      { name: strOpportunities, image: "" },
      { name: strKPIsList, image: "" },
      { name: strSwot, image: "" },
      { name: strHelp, image: "" }
    ];

    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <ScrollView>
          <View style={practiceStyle.date}>
            <PracticeIdView onPracticeChanged={this.onPracticeChanged.bind(this)} />
          </View>

          <FlatGrid
            itemDimension={Dimensions.get("window").width / 2 - 20}
            items={items}
            style={styles.gridView}
            renderItem={({ item, index }) => (
              <TouchableWithoutFeedback
                onPress={this.homeItemTapped.bind(this, item.name)}
              >
                <View style={styles.itemContainer, this.styleimage(item.name)}>
                  <Text style={styles.itemImage}>{item.image}</Text>
                  <Text style={this.styleitem(item.name)}>{item.name}</Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          />
        </ScrollView>
      </View>
    );

  }
  onPracticeChanged(practiceSelected) {
    if (practiceSelected != null) {
      this.setState({
        selectedPractice: practiceSelected
      });
      if (practiceSelected.accountTypeId != undefined) {
        if (practiceSelected.accountTypeId == 2) {
          global.IsSelectedLite = true
        }
        else {
          global.IsSelectedLite = false
        }
      }

    }
  }
}

const practiceStyle = {
  date: {
    backgroundColor: "white",
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 40,
    marginRight: 40,
    height: 50,
    shadowRadius: 14,
    shadowOpacity: 1,
    justifyContent: "center",
    shadowOffset: { width: 4, height: 4 },
    shadowColor: "rgba(0.14, 0.14, 0.17, 0.07)",
    borderRadius: 5,
    elevation: 2
  }
};
export default withNavigation(Home);
