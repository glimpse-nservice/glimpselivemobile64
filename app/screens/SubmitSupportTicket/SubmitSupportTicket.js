import React from 'react';
import { View, Text, KeyboardAvoidingView, ScrollView, TextInput, Dimensions, TouchableOpacity, Keyboard, Linking } from 'react-native';
import styles from '../Login/LoginStyle';
import { Strings } from '../../config/Strings';
import Constants from '../../config/Constants';
import { Spinner, Header } from '../../components/common';
import PracticeDropDown from '../../components/PracticesDropDown';

const { screenContainerStyle, inputTagStyle, inputStyle, loginButtonStyle, errorTextStyle } = styles;
const { strEmail, strEnterEmail, strSubject, strTicketInfo, strPractice, strSendEmail, strEmailError, strTicketError } = Strings.submitSupportTicket;

class SubmitSupportTicket extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle={Strings.drawer.strSubmitSupportTicket} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  constructor() {
    super();
    this.state = { email: '', practice: null, subject: `Mobile Customer Support:- ${new Date()}`, ticket: '', loading: false };
  }

  /**
   * This function is called when user selects the practice
   * from the drop down
   * @param {practice info} practiceSelected
   */
  onPracticeSelected(practiceSelected) {
    console.log(practiceSelected);

    if (practiceSelected !== null) {
      this.setState({
        practice: practiceSelected
      });
    }
  }

  /**
   * This function is called when send email button is clicked.
   * it checks if user has entered information or not, if not shows
   * error to the user. If yes, then sends email to
   * glimpse support.
   */
  onBtnClicked() {
    Keyboard.dismiss();

    if (this.state.email === undefined || this.state.email === '') {
      this.setState({ error: strEmailError, loading: false });
      return;
    }

    if (this.state.ticket === undefined || this.state.ticket === '') {
      this.setState({ error: strTicketError, loading: false });
      return;
    }

    const emailBody = `Ticket Information:- \n${this.state.ticket}\n\nMentioned Email:- ${this.state.email}\n\nPractice:- ${this.state.practice.label}`;

    Linking.openURL(`mailto:${Strings.contactUs.strSupportEmail}?subject=${this.state.subject}&body=${emailBody}`);
  }

  /**
   * This function renders the email button or
   * loader.
   */
  renderEmailButton() {
    if (this.state.loading) {
      return (
        <View style={{ marginTop: 20 }}>
          <Spinner flag="login" />
        </View>
      );
    }
    return (
      <TouchableOpacity style={loginButtonStyle} onPress={() => this.onBtnClicked()}>
        <Text style={{ color: 'white', fontFamily: 'Roboto-Bold' }}>{strSendEmail.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <KeyboardAvoidingView enabled style={{ flex: 1 }}>
        <View style={screenContainerStyle}>
          <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always" contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
            {/*Submit support ticket Form*/}
            <View style={{ marginTop: 20 }}>
              {/*Email*/}
              <Text style={inputTagStyle}>{strEmail.toUpperCase()}</Text>
              <TextInput
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                placeholder={strEnterEmail}
                placeholderTextColor="#C5CBD9"
                keyboardType={'email-address'}
                style={inputStyle}
                label={strEnterEmail}
                value={this.state.email}
                onChangeText={email => {
                  this.setState({ email, error: '' });
                }}
              />

              {/*Subject*/}
              <Text style={inputTagStyle}>{strSubject.toUpperCase()}</Text>

              <View style={practiceStyle.subjectTextStyle}>
                <Text
                  style={{
                    fontFamily: 'Roboto-Regular',
                    fontSize: 14,
                    color: '#394263',
                    padding: 5
                  }}
                  multiline
                >
                  {this.state.subject}
                </Text>
              </View>

              <Text style={inputTagStyle}>{strPractice.toUpperCase()}</Text>
              <View style={practiceStyle.date}>
                <PracticeDropDown onPracticeSelected={this.onPracticeSelected.bind(this)} />
              </View>

              {/*Ticket info*/}
              <Text style={inputTagStyle}>{strTicketInfo.toUpperCase()}</Text>
              <TextInput
                underlineColorAndroid="transparent"
                placeholderTextColor="#C5CBD9"
                style={[inputStyle, { height: 80, textAlignVertical: 'top' }]}
                multiline
                numberOfLines={4}
                maxLength={1000}
                label={strTicketInfo}
                value={this.state.ticket}
                onChangeText={ticket => {
                  this.setState({ ticket, error: '' });
                }}
              />

              {/*Error message*/}
              <Text style={errorTextStyle}>{this.state.error}</Text>
              {/*Send Email button*/}
              {this.renderEmailButton()}
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const practiceStyle = {
  date: {
    backgroundColor: 'white',
    marginTop: 10,
    height: 50,
    marginBottom: 5,
    shadowRadius: 14,
    shadowOpacity: 1,
    justifyContent: 'center',
    shadowOffset: { width: 4, height: 4 },
    shadowColor: 'rgba(0.14, 0.14, 0.17, 0.07)',
    borderColor: '#D5D9E2',
    borderWidth: 1,
    borderRadius: 5
  },
  subjectTextStyle: {
    borderColor: '#D5D9E2',
    borderWidth: 1,
    borderRadius: 5,
    width: Dimensions.get('window').width - 40,
    marginTop: 10
  }
};

export default SubmitSupportTicket;
