import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export default {
  headerTextStyle: {
    fontFamily: 'Roboto-Bold',
    color: '#394263',
    fontSize: width * 0.04,
    marginTop: 20
  },
  metricCardStyle: {
    backgroundColor: 'white',
    elevation: 3,
    padding: 5,
    borderRadius: 5,
    marginTop: 10,
    shadowRadius: 12,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0, 0, 0, 0.18)'
  },
  metricColumnsTextStyle: {
    fontSize: width * 0.03,
    color: '#394263',
    padding: 3,
    fontFamily: 'Roboto-Bold'
  },
  metricRowsTextStyle: {
    fontSize: width * 0.03,
    color: '#394263',
    padding: 3,
    fontFamily: 'Roboto-Regular'
  }
};
