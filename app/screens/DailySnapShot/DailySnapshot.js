import React, { Component } from "react";
import Snackbar from "react-native-snackbar";
import {
  View,
  Text,
  ScrollView,
  FlatList,
  Image,
  TouchableOpacity
} from "react-native";
import Constants from "../../config/Constants";
import { CalendarLocationView, Spinner, Header } from "../../components/common";
import WebService, { URL_DAILY_SNAPSHOTS } from "../../webservice";
import styles from "../DailySnapShot/DailySnapshotStyles";
import {
  getFormattedDate,
  numberWithCommas,
  formatNumber
} from "../../config/Utility";
import { UserData } from "../../config/UserData";
import { Strings } from "../../config/Strings";
import demoData from "../../data/StaticSnapData";

const imgDown = require("../../images/down.png");
const imgUp = require("../../images/UP.png");
const imgZeroGrowth = require("../../images/growthZero.png");
//let locationType = null;
let isMounted = false;

let practiceId = null;

export default class DailySnapShot extends Component {
  constructor() {
    super();

    const today = getFormattedDate(new Date());
    this.state = {
      data: [],
      loading: false,
      date: today,
      location: "",
      disableSelector: false
    };
  }

  componentDidMount() {
    isMounted = true;
  }

  componentWillUnmount() {
    isMounted = false;
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <Header headerTitle={Strings.dailySnapshot.strDailySnapshot} />
      ),
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  /**
   * This function fetch data from server
   * according to the location and date set by
   * the user
   */
  getDataFromServer() {
    this.setState({ loading: true, disableSelector: true });

    //check to see if demo site
    /*     UserData.getUserLocations().then(info => {
      const userLocations = JSON.parse(info);
      for (i in userLocations) {
        locationType = userLocations[i].typeId
      }

    });
 */
    UserData.getUserSelectedPractice()
      .then(res => {
        const userPractice = JSON.parse(res);
        practiceId = userPractice.value;

        /*      if(locationType == 2){
         
     
            this.groupCategoryAndMetrics(demoData);
          
        }
              else { */

        const apiURL = URL_DAILY_SNAPSHOTS(
          practiceId,
          this.state.location,
          this.state.date
        );

        WebService.get(apiURL)
          .then(response => {
            if (isMounted) {
              this.groupCategoryAndMetrics(response.data[0]);
            }
          })
          .catch(error => {
            Snackbar.show({
              title: error.message,
              duration: Snackbar.LENGTH_SHORT
            });
            this.setState({ loading: false, disableSelector: false });
          });
        //}
      })
      .catch(() => {
        Snackbar.show({
          title: "Unable to fetch data from server. Please try again",
          duration: Snackbar.LENGTH_SHORT
        });
        this.setState({ loading: false, disableSelector: false });
      });
  }

  /**
   * This function groups the response according
   * to the metric category
   * @param {server} response
   */
  groupCategoryAndMetrics(response) {
    if (response === null) {
      return;
    }

    const categoryList = [];
    const dailySnapShotJson = [];

    response.metrics.forEach(item => {
      if (categoryList.indexOf(item.category) <= -1) {
        categoryList.push(item.category);

        const categoryObj = {
          name: item.category,
          metrics: []
        };
        dailySnapShotJson.push(categoryObj);
      }

      dailySnapShotJson.forEach(metric => {
        if (metric.name === item.category) {
          const metricObj = {
            key: item.key,
            label: item.label,
            toolTip: item.tooltip,
            dataType: item.dataType,
            current: item.data[0].data,
            previous: item.prev[0].data,
            growth: item.growth
          };
          metric.metrics.push(metricObj);
        }
      });
    });

    this.setState({
      data: dailySnapShotJson,
      loading: false,
      disableSelector: false
    });
  }

  /**
   * This function renders the metrics on the
   * screen, by mapping the list of metrics available under
   * each group. this function also calculates the growth
   * percentage whether increase or decrease.
   * @param {Metric data} metric
   */
  renderMetrics(metric) {
    const item = metric.item;

    let itemCurrent = null;
    let itemPrev = null;

    switch (item.dataType) {
      case "currency":
        itemCurrent = `$${numberWithCommas(formatNumber(item.current))}`;
        itemPrev = `$${numberWithCommas(formatNumber(item.previous))}`;
        break;
      case "pct":
        itemCurrent = `${item.current.toFixed(1)}%`;
        itemPrev = `${item.previous.toFixed(1)}%`;
        break;
      case "units":
        itemCurrent = `${item.current}`;
        itemPrev = `${item.previous}`;
        break;
      default:
    }

    const growthPct = item.growth;

    let growthIcon = "";

    if (growthPct === 0) {
      growthIcon = imgZeroGrowth;
    } else {
      growthIcon = growthPct > 0 ? imgUp : imgDown;
    }

    return (
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        {/*Metric name*/}
        <Text
          style={[{ flex: 2, textAlign: "left" }, styles.metricRowsTextStyle]}
        >
          {item.label}
        </Text>

        {/*Metric curent value*/}
        <Text
          style={[{ flex: 1, textAlign: "right" }, styles.metricRowsTextStyle]}
        >
          {itemCurrent}
        </Text>

        {/*Metric growth*/}
        <Text
          style={[{ flex: 1, textAlign: "right" }, styles.metricRowsTextStyle]}
        >{`${growthPct.toFixed(1)}%`}</Text>
        <Image style={{ height: 12, width: 12 }} source={growthIcon} />

        {/*Metric previous value*/}
        {/*  <Text style={[{ flex: 1, textAlign: 'right' }, styles.metricRowsTextStyle]}>{itemPrev}</Text> */}
      </View>
    );
  }

  /**
   * This function renders data that is fetched
   * from the api, on the screen
   */
  renderDataOnScreen(item) {
    return (
      <View key={item.name}>
        <Text style={styles.headerTextStyle}>
          {item.name
            .substr(0, 1)
            .toUpperCase()
            .concat(item.name.substr(1))}
        </Text>

        <View style={styles.metricCardStyle}>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={[
                { flex: 2, textAlign: "left" },
                styles.metricColumnsTextStyle
              ]}
            >
              {" "}
              Metric{" "}
            </Text>
            <Text
              style={[
                { flex: 1, textAlign: "right" },
                styles.metricColumnsTextStyle
              ]}
            >
              {" "}
              Data{" "}
            </Text>
            <Text
              style={[
                { flex: 1, textAlign: "right" },
                styles.metricColumnsTextStyle
              ]}
            >
              {" "}
              Growth{" "}
            </Text>
            {/* <Text style={[{ flex: 1, textAlign: 'right' }, styles.metricColumnsTextStyle]}> Previous </Text> */}
          </View>

          <View style={{ backgroundColor: "#A1A1A1", height: 1 }} />

          {/*Metrics*/}
          <FlatList
            data={item.metrics}
            keyExtractor={metric => metric.key}
            renderItem={metric => this.renderMetrics(metric)}
          />
        </View>
      </View>
    );
  }

  /**
   * This function is called whenever user selects a date
   * @param {new date that is selected by the user } newDate
   */
  onDateOrLocationSet(newDate, newLocation) {
    if (newDate !== this.state.date || newLocation !== this.state.location) {
      this.setState(
        {
          date: newDate,
          location: newLocation
        },
        () => {
          this.getDataFromServer();
        }
      );
    }
  }

  /**
   * This function shows loader on the screen
   * whenever data is being fetched from the server
   */
  showSpinner() {
    if (this.state.loading) {
      return <Spinner />;
    }

    return (
      <ScrollView>
        <View style={{ margin: 15 }}>
          {this.state.data.map(item => this.renderDataOnScreen(item))}
        </View>
      </ScrollView>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <CalendarLocationView
          disabled={this.state.disableSelector}
          onDateOrLocationSet={this.onDateOrLocationSet.bind(this)}
          initialDateYesterday
        />

        {this.showSpinner()}
      </View>
    );
  }
}
