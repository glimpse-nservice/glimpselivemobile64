import { Dimensions } from 'react-native';
import Constants from '../../config/Constants';

const { screenWidth, screenHeight } = Dimensions.get('window');

export default {
  screenContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    paddingStart: 30,
    paddingEnd: 30,
    backgroundColor: 'white'
  },
  glimpseLogoStyle: {
    alignSelf: 'center',
  },

  signInTagStyle: {
    fontSize: 30,
    color: '#394263',
    fontFamily: 'Roboto-Regular'
  },
  enterDetailTagStyle: {
    color: '#717A8A',
    fontSize: 16,
    marginTop: 10,
    fontFamily: 'Roboto-Regular'
  },
  inputTagStyle: {
    color: '#394263',
    marginTop: 20,
    fontSize: 13,
    fontFamily: 'Roboto-Medium'
  },
  forgotPwdTagStyle: {
    color: '#8B96AA',
    fontSize: 14,
    marginTop: 20,
    fontFamily: 'Roboto-Regular'
  },
  inputStyle: {
    borderColor: '#D5D9E2',
    borderWidth: 1,
    borderRadius: 5,
    height: 40,
    marginTop: 10,
  },
  loginButtonStyle: {
    marginTop: 16,
    marginLeft: 24,
    marginRight: 24,
    backgroundColor: Constants.appColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 25
  },
  errorTextStyle: {
    color: 'red',
    fontSize: 15,
    alignSelf: 'center',
    marginTop: 10
  }
};
