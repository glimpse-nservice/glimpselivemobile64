import React, { Component } from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  YellowBox,
  Image,
  Keyboard
} from "react-native";
import md5 from "md5";
import { Strings } from "../../config/Strings";
import { onLogIn } from "../../helper/auth";
import { UserData } from "../../config/UserData";
import WebService, { URL_LOGIN } from "../../webservice";
import { Spinner } from "../../components/common";
import styles from "./LoginStyle";

import { Button, Input, Icon } from "react-native-elements";

const imageIcon = require("../../images/glimpse-logo-md.png");

const {
  strEnterDetailsTag,
  strUserName,
  strPassword,
  strForgotPwd,
  strEnterYourUsername,
  strSignIn,
  strEnterYourPwd,
  strLoginError,
  strUsernameError,
  strPwdError
} = Strings.login;

const {
  screenContainerStyle,
  glimpseLogoStyle,
  enterDetailTagStyle,
  inputTagStyle,
  forgotPwdTagStyle,
  inputStyle,
  loginButtonStyle,
  errorTextStyle
} = styles;

YellowBox.ignoreWarnings(["Warning: isMounted(...) is deprecated"]);

export default class Login extends Component {
  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      userName: "",
      password: "",
      error: "",
      loading: false,
      isSecureTextEntry: true
    };
  }

  /**
   * This function is called when user clicks login button.
   * This function validates the login form and after successful
   * validation, calls login api and logs user in.
   */
  onLoginButtonClicked() {
    Keyboard.dismiss();
    if (this.state.userName === undefined || this.state.userName === "") {
      this.setState({ error: strUsernameError, loading: false });
      return;
    }

    if (this.state.password === undefined || this.state.password === "") {
      this.setState({ error: strPwdError, loading: false });
      return;
    }

    this.setState({ loading: true });
    //then check to see if security questions are filled out
    WebService.get(
      URL_LOGIN.concat(this.state.userName).concat(
        `/${md5(this.state.password)}/`
      )
    )

      .then(response => {
        if (
          response.data.user.securityQuestionId1 == 0 ||
          response.data.user.securityQuestionId2 == 0
        ) {
          this.setState({ loading: false });
          UserData.storeUserLoginId(response.data.user.loginId);
          UserData.storeUserName(`${response.data.user.userName}`);
          UserData.storeUserPractices(response.data.dataAccess.practice);
          UserData.storeUserLocations(response.data.dataAccess.location);
          this.props.navigation.navigate("NewUserForm");
        } else {
          UserData.storeUserLoginId(response.data.user.loginId);
          UserData.storeUserName(`${response.data.user.strUserName}`);
          UserData.storeUserPractices(response.data.dataAccess.practice);
          UserData.storeUserLocations(response.data.dataAccess.location);
          onLogIn().then(() => this.props.navigation.navigate("LoggedIn"));
        }
      })
      .catch(error => {
        if (error.response) {
          if (error.response.data.Message === "Account is locked") {
            this.setState({
              error:
                "Account is locked. Please contact customer Service at 1-888-413-5245 ",
              loading: false
            });
          } else {
            this.setState({
              error: strLoginError,
              loading: false
            });
          }
        }
      });
  }

  /**
   * This function renders the sign in button or
   * loader.
   */
  renderSignInButton() {
    if (this.state.loading) {
      return (
        <View style={{ marginTop: 20 }}>
          <Spinner flag="login" />
        </View>
      );
    }
    return (
      <Button
        onPress={this.onLoginButtonClicked.bind(this)}
        clear
        title={strSignIn.toUpperCase()}
        icon={
          <Icon name="sign-in" color="white" type="font-awesome" size={18} />
        }
        titleStyle={{ color: "white", fontFamily: "Roboto-Bold" }}
        buttonStyle={{
          flexDirection: "row",
          borderRadius: 25,

          height: 50,
          flex: 1
        }}
        containerStyle={loginButtonStyle}
      />
    );
  }

  onShowPassword() {
    const previousState = this.state.isSecureTextEntry;
    requestAnimationFrame(() => {
      this.setState({
        isSecureTextEntry: !previousState
      });
    });
  }

  render() {
    return (
      <KeyboardAvoidingView enabled style={{ flex: 1 }}>
        <View style={screenContainerStyle}>
          <ScrollView
            keyboardShouldPersistTaps="always"
            contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
          >
            <Image style={glimpseLogoStyle} source={imageIcon} />

            {/*Login Form*/}
            <View style={{ marginTop: 40, justifyContent: "center" }}>
              <Text style={enterDetailTagStyle}>{strEnterDetailsTag}</Text>
              {/*username*/}
              <Input
                inputContainerStyle={inputStyle}
                placeholder={strEnterYourUsername}
                autoCapitalize="none"
                leftIcon={
                  <Icon
                    name="user"
                    type="simple-line-icon"
                    size={18}
                    color="#8B96AA"
                  />
                }
                underlineColorAndroid="transparent"
                placeholderTextColor="#C5CBD9"
                label={strUserName.toUpperCase()}
                labelStyle={inputTagStyle}
                value={this.state.userName}
                onChangeText={userName => {
                  this.setState({ userName, error: "" });
                }}
              />

              {/*Password*/}
              <Input
                placeholder={strEnterYourPwd}
                autoCapitalize="none"
                secureTextEntry={this.state.isSecureTextEntry}
                leftIcon={
                  <Icon
                    name={
                      this.state.isSecureTextEntry
                        ? "eye-outline"
                        : "eye-off-outline"
                    }
                    type="material-community"
                    size={18}
                    color="#8B96AA"
                    onPress={() => this.onShowPassword()}
                  />
                }
                underlineColorAndroid="transparent"
                placeholderTextColor="#C5CBD9"
                labelStyle={inputTagStyle}
                inputContainerStyle={inputStyle}
                label={strPassword.toUpperCase()}
                value={this.state.password}
                onChangeText={password => {
                  this.setState({ password, error: "" });
                }}
              />
              {/*Forgot Password*/}
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("UpdatePasswordForm");
                }}
              >
                <Text style={forgotPwdTagStyle}>{strForgotPwd}</Text>
              </TouchableOpacity>

              {/*Error message*/}
              <Text style={errorTextStyle}>{this.state.error}</Text>
              {/*Login button*/}
              {this.renderSignInButton()}
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
