import React, { Component } from 'react';
import { View, FlatList, Text, Switch, TouchableOpacity } from 'react-native';
import Constants from '../../config/Constants';
import { Strings } from '../../config/Strings';
import styles from './NotificationsStyle';
import { Header } from '../../components/common/index';

const { strNotifications, strSwotReport, strMonthlyMissedOpportunities, strEngagement } = Strings.notifications;

export default class Notifications extends Component {
  constructor() {
    super();

    this.state = { switch: false };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle={strNotifications} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  /**
   * This function renders separator for the list
   * items
   */
  renderSeparator = () => (
    <View
      style={{
        backgroundColor: '#EBEBEB',
        height: 1
      }}
    />
  );

  render() {
    const data = [{ name: strSwotReport }, { name: strMonthlyMissedOpportunities }, { name: strEngagement }];

    const { container, itemContainer, text, switchStyle } = styles;
    return (
      <View style={container}>
        <FlatList
          data={data}
          ItemSeparatorComponent={this.renderSeparator}
          renderItem={({ item }) => {
            return (
              <View style={itemContainer}>
                <Text style={text}>{item.name}</Text>
                <Switch
                  style={switchStyle}
                  onTintColor={Constants.appColor}
                  thumbTintColor={Constants.appColor}
                  onValueChange={value => {
                    this.setState({ switch: value });
                  }}
                  value={this.state.switch}
                />
              </View>
            );
          }}
          keyExtractor={item => item.name}
        />
      </View>
    );
  }
}
