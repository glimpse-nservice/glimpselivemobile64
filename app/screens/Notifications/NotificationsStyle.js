export default {
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    color: '#394263',
    padding: 16,
    fontFamily: 'Roboto-Regular'
  },
  switchStyle: {
    position: 'absolute',
    right: 0,
    marginRight: 16
  }
};
