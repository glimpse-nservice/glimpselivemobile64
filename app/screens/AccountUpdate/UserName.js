/* eslint-disable no-sequences */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-return-assign */
/* eslint-disable no-undef */
import React, { Component } from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import { Text, Card, Input } from 'react-native-elements';

import { Strings } from '../../config/Strings';
import styles from './AccountUpdateStyles';


const { inputTagStyle, inputStyle, headerStyle } = styles;

export default class Username extends Component {

    constructor(props) {
        super(props);
        this.state = {
            UserName: ''
        };
    }


    _onChangeText = (UserName) => {
        this.props.onChangeValue('UserName', UserName.trim());
    };

    render() {
        return (
            <KeyboardAvoidingView enabled={false} style={{ flex: 1, backgroundColor: '#fff' }} >
            <Card
title={Strings.updateUsers.strForgetPassword}
                containerStyle={{ borderWidth: 0, elevation: 0 }}
            >
                <Text style={headerStyle} > {Strings.updateUsers.strUserIntro}</Text>
                <View style={alignSelf = 'center'}>


                    {/*userName*/}
                    <Input
                        placeholder={Strings.login.userName}
                        autoCapitalize="none"
                        underlineColorAndroid="transparent"
                        placeholderTextColor="#C5CBD9"
                        labelStyle={inputTagStyle}
                        inputContainerStyle={inputStyle}
                        label={Strings.login.strUserName.toUpperCase()}
                        onChangeText={UserName => { this.setState({ UserName }), this._onChangeText(UserName); }}
                    />
                </View>

                </Card>
                </KeyboardAvoidingView>
        );
    }

}

