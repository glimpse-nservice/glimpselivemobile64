/* eslint-disable import/newline-after-import */
/* eslint-disable no-useless-constructor */
/* eslint-disable no-return-assign */
/* eslint-disable no-undef */
import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  BackHandler
} from "react-native";
import Wizard from "../../wizard/index";
import UserName from "../AccountUpdate/UserName";
import UserSecurityQuestion from "./UserSecurityQuestions";
import NewPassword from "./NewPassword";
import Constants from "../../config/Constants";

//TOD0 -- move brand logo header to a common file
const BrandLogo = require("../../images/glimpse_brand_logo_header.png");
const styles = {
  screenContainerStyle: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "white"
  }
};
export default class UpdatePasswordForm extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: <View />,
      headerRight: <View />,
      // headerRight: (
      //   //  <TouchableOpacity onPress={navigation.openDrawer}>
      //   <TouchableOpacity >
      //     <Text style={Constants.sideMenuStyle}></Text>
      //   </TouchableOpacity>
      // ),
      headerTitle: (
        <View style={{ flex: 1 }}>
          <Image style={{ alignSelf: "center" }} source={BrandLogo} />
        </View>
      ),
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerBackTitle: null
    };
  };

  render() {
    return (
      <KeyboardAvoidingView
        behavior="padding"
        enabled={false}
        style={{ flex: 1, backgroundColor: "#fff" }}
      >
        <View style={(style = styles.screenContainerStyle)}>
          <ScrollView
            keyboardShouldPersistTaps="always"
            contentContainerStyle={{ flexGrow: 1, justifyContent: "center" }}
          >
            <Wizard
              title={"UpdatePassword"}
              initialValues={{
                UserName: "",

                UserSecurityQuestion: {
                  question1Id: "",
                  question1Answer: "",
                  question2Id: "",
                  question2Answer: "",
                  loginId: 0
                },
                NewPassword: {
                  password: "",
                  confirmPassword: "",
                  errors: []
                }
              }}
              navigation={this.props.navigation}
            >
              <Wizard.Step>
                {({ onChangeValue, values }) => (
                  <UserName
                    onChangeValue={onChangeValue}
                    value={values.UserName}
                  />
                )}
              </Wizard.Step>
              <Wizard.Step>
                {({ onChangeValue, values }) => (
                  <UserSecurityQuestion
                    onChangeValue={onChangeValue}
                    value={values.UserSecurityQuestion}
                  />
                )}
              </Wizard.Step>

              <Wizard.Step>
                {({ onChangeValue, values }) => (
                  <NewPassword
                    onChangeValue={onChangeValue}
                    value={values.NewPassword}
                  />
                )}
              </Wizard.Step>
            </Wizard>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
  componentDidMount() {
    if (Platform.OS === "android") {
      BackHandler.addEventListener("hardwareBackPress", this.onUpdatePasswordBackPressed);
    }
  }

  onUpdatePasswordBackPressed = () => {
    this.props.navigation.navigate('LoggedOut');
  }
  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener("hardwareBackPress", this.onUpdatePasswordBackPressed);
    }
  }
}
