import Constants from '../../config/Constants';

export default {

    inputTagStyle: {
        color: '#394263',
        marginTop: 20,
        fontSize: 13,
        fontFamily: 'Roboto-Medium'
    },
    inputStyle: {
        borderColor: '#D5D9E2',
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        marginTop: 15,
        padding: 10,
        width: '100%'
    },

    securityInputStyle: {
        borderColor: '#D5D9E2',
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        marginTop: 15,
        padding: 10,
        width: '105%',
        marginLeft: 10
    },
    headerStyle: {
        color: '#394263',
        fontWeight: '600',
        fontSize: 15,
        fontFamily: 'Roboto-Medium',
        alignSelf: 'center',
        marginBottom: 15,

    },

    instructCheckBoxConatiner: {
        height: 5,
        backgroundColor: '#fff',
        borderWidth: 0
    },

    instructStyle: {
        fontWeight: '200'

    },

    picker: {
        width: '100%',

        backgroundColor: '#f2f2f2',
        borderColor: '#394263',
        borderBottomWidth: 2,
        flex: 90,
        margin: 10
    },

    pickerItem: {
        height: 20,
        color: 'red'
    },

    headerText: {
        paddingTop: 24,
        paddingBottom: 24,
        paddingHorizontal: 24,
        color: '#394263',
        fontFamily: 'Roboto-Regular',
        fontWeight: '500',
        fontSize: 18,
        textAlign: 'center',

    },
    buttonStyle: {
        marginTop: 16,
        marginLeft: 24,
        marginRight: 24,
        backgroundColor: Constants.appColor,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderRadius: 25
    },

    errorText: {

        fontFamily: 'Roboto-Regular',
        fontWeight: '500',
        fontSize: 15,
        padding: 15,
        color: 'red'
    },

    buttonWrapper: {
        marginTop: 16,
        marginLeft: 24,
        marginRight: 24,
        backgroundColor: Constants.appColor,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderRadius: 25
    }

};
