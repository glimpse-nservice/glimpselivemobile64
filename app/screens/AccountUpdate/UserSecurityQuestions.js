import React, { Component } from 'react';
import { View, Alert, Text } from 'react-native';
import { Card, Input, Icon } from 'react-native-elements';
import { Strings } from '../../config/Strings';
import styles from './AccountUpdateStyles';
import WebService, { URL_USER_SECURITY_QUESTIONS } from '../../webservice';
import { UserData } from '../../config/UserData';


const { securityInputStyle, inputStyle, picker, pickerItem, headerStyle, headerText } = styles;


export default class UserSecurityQuestion extends Component {

  constructor() {
    super();

    this.state = { data: [], loading: false, loginId: 0, question1Id: '', question2Id: '', Answer1: '', Answer2: '' };
  }
  componentDidMount() {

    UserData.getUserName().then(userName =>

      WebService.get(URL_USER_SECURITY_QUESTIONS(userName))
        .then((response) => {
          var jsonData = response.data
          this.setState({ isLoading: false, data: jsonData.user });
        })
        .catch((error) => {
          Alert.alert("Error",
            "User not Found. Try Again")
        })


    )
  }

  _onChangeText = (Answer1, Answer2) => {
    var UserSecurityQuestion = {}

    UserSecurityQuestion = {
      question1Id: this.state.data.securityQuestionId1,
      question1Answer: Answer1,
      question2Id: this.state.data.securityQuestionId2,
      question2Answer: Answer2,
      loginId: this.state.data.loginId
    }

    this.props.onChangeValue("UserSecurityQuestion", UserSecurityQuestion)

  };


  render() {

    return (
      <Card title={Strings.updateUsers.strVerifyTitle}
        containerStyle={{ borderWidth: 0, elevation: 0, alignContent: 'flex-start' }}
      >
        <View>
        <Text style={headerStyle} > {Strings.updateUsers.strVerifyInstruct}</Text>
          <Input
            label={this.state.data.securityQuestion1}
            placeholder={Strings.updateUsers.strAnswer}
            autoCapitalize="none"
            placeholderTextColor="#C5CBD9"
            inputContainerStyle={securityInputStyle}
            onChangeText={Answer1 => { this.setState({ Answer1 }), this._onChangeText(Answer1, this.state.Answer2) }}
          />
          <Input
            label={this.state.data.securityQuestion2}
            placeholder={Strings.updateUsers.strAnswer}
            autoCapitalize="none"
            placeholderTextColor="#C5CBD9"
            inputContainerStyle={securityInputStyle}
            onChangeText={Answer2 => { this.setState({ Answer2 }), this._onChangeText(this.state.Answer1, Answer2) }}
          />
        </View>

      </Card>
    )
  }

}