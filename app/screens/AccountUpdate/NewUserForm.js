import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, KeyboardAvoidingView, Image } from 'react-native';
import Wizard from '../../wizard';
import NewPassword from './NewPassword';
import SecurityQuestion from './NewUserSecurityQuestions';
import Constants from '../../config/Constants';


const BrandLogo = require('../../images/glimpse_brand_logo_header.png');


const styles = {

  screenContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
}
export default class NewUserForm extends Component {
  constructor(props) {
    super(props);

  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: <View />,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      ),
      headerTitle: <View style={{ flex: 1 }}>
        <Image style={{ alignSelf: 'center' }} source={BrandLogo} />
      </View>,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerBackTitle: null
    };
  };




  render() {

    return (

      <KeyboardAvoidingView enabled={false} behavior="padding" style={{ flex: 1, backgroundColor: '#fff' }} >
        <View style={style = styles.screenContainerStyle}>

          <ScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
            <Wizard title={"NewUserForm"}
              initialValues={{
                NewPassword: {
                  password: '',
                  confirmPassword: '',
                  errors: []
                },
                SecurityQuestion: {
                  question1Id: '',
                  question1Answer: '',
                  question2Id: '',
                  question2Answer: ''
                }
              }}
              navigation={this.props.navigation}

            >


              <Wizard.Step>
                {({ onChangeValue, values }) => (
                  <NewPassword
                    onChangeValue={onChangeValue}
                    value={values.NewPassword}

                  />
                )}
              </Wizard.Step>
              <Wizard.Step>
                {({ onChangeValue, values }) => (
                  <SecurityQuestion
                    onChangeValue={onChangeValue}
                    value={values.SecurityQuestion}
                  />
                )}
              </Wizard.Step>
            </Wizard>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>

    );
  }
}