import React, { Component } from 'react';
import { View, Picker } from 'react-native';
import { Card, Input } from 'react-native-elements';
import { Strings } from '../../config/Strings';
import styles from './AccountUpdateStyles';
import WebService, { URL_SECURITY_QUESTIONS } from '../../webservice';

const { securityInputStyle, picker, pickerItem } = styles;


export default class NewUserSecurityQuestion extends Component {

  constructor() {
    super();

    this.state = { data: [], loading: false, question1Id: '', question2Id: '', Answer1: '', Answer2: '' };
  }
  componentDidMount() {
    WebService.get(URL_SECURITY_QUESTIONS)
      .then((response) => {
        var jsonData = response.data
        this.setState({
          isLoading: false,
          data: jsonData


        }, function () {
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  _onChangeText = (Answer1, Answer2) => {
    var SecurityQuestion = {}
 
    SecurityQuestion = {
      question1Id: this.state.question1Id,
      question1Answer: Answer1,
      question2Id: this.state.question2Id,
      question2Answer: Answer2
    }

    this.props.onChangeValue("SecurityQuestion", SecurityQuestion)

  };


  render() {
    return (
      <Card title={Strings.updateUsers.strQuestionTitle}
        containerStyle={{ borderWidth: 0, elevation: 0, alignContent: 'flex-start' }}
      >
        <View style={{ alignContent: 'center' }}>
          {/*Question 1*/}

          <Picker
            selectedValue={this.state.question1Id}
            prompt={"Select a Question"}
            style={[picker]}
            itemStyle={pickerItem}
            mode={'dropdown'}
            onValueChange={(itemValue) => this.setState({ question1Id: itemValue })} >
            <Picker.Item label="Select a Question" value="" />
            {this.state.data.map((item, key) => (
              <Picker.Item label={item.question} value={item.id} key={key} />)
            )}
          </Picker>
          <Input
            placeholder={Strings.updateUsers.strAnswer}
            autoCapitalize="none"
            placeholderTextColor="#C5CBD9"
            inputContainerStyle={securityInputStyle}
            onChangeText={Answer1 => { this.setState({ Answer1 }), this._onChangeText(Answer1, this.state.Answer2) }}
          />
          {/*Question 2*/}

          <Picker
            selectedValue={this.state.question2Id}
            prompt={"Select a Question"}
            style={[picker]}
            itemStyle={pickerItem}
            mode={'dropdown'}
            onValueChange={(itemValue) => this.setState({ question2Id: itemValue })} >
            <Picker.Item label="Select a Question" value="" />
            {this.state.data.map((item, key) => (
              <Picker.Item label={item.question} value={item.id} key={key} />)
            )}
          </Picker>
          <Input
            placeholder={Strings.updateUsers.strAnswer}
            autoCapitalize="none"
            placeholderTextColor="#C5CBD9"
            inputContainerStyle={securityInputStyle}
            onChangeText={Answer2 => { this.setState({ Answer2 }), this._onChangeText(this.state.Answer1, Answer2) }}
          />
        </View>

      </Card>
    )
  }

}