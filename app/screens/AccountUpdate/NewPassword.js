import React, { Component } from 'react';
import { View } from 'react-native';
import { CheckBox, Text, Card, Input, Icon } from 'react-native-elements';

import { Strings } from '../../config/Strings';
import styles from './AccountUpdateStyles';


const { inputTagStyle, inputStyle, headerStyle, instructCheckBoxConatiner, instructStyle } = styles;
var errorMessage = [];


export default class NewPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isSecureTextEntry: true,
            password: '',
            errorMessage: [],
            confirmPassword: '',
            loading: false,
            isLowerCase: false,
            isDigit: false,
            isLength: false,
            isUpperCase: false,


        };
    }

    _onChangeText = (password, confirmPassword) => {
        var NewPassword = {}

        //console.log(password + ',' + confirmPassword)

        var error = (this.validate(password, confirmPassword))
        if (error === 0) {
            NewPassword = {
                "password": password,
                "confirmPassword": confirmPassword,
                "errors": []
            }
        }
        else {

            NewPassword = {
                "password": " ",
                "confirmPassword": " ",
                "errors": errorMessage
            }
        }


        this.props.onChangeValue("NewPassword", NewPassword)

    };


    validate(password, confirmPassword) {
        var error = 0;

        if (password !== confirmPassword) {
            error = 1;
            this.addErrorMessage('Passwords Do Not Match');
        }
        else {

            this.removeErrorMessage('Passwords Do Not Match')
        }

        if (password.match(/[a-z]/g)) {
            this.setState({ isLowerCase: true });
            this.removeErrorMessage('Password must contain at least one lower case letter')
        }
        else {
            error = 1;
            this.addErrorMessage('Password must contain at least one lower case letter');
            this.setState({ isLowerCase: false });
        }

        if (password.match(/[A-Z]/g)) {
            this.setState({ isUpperCase: true })
            this.removeErrorMessage('Password must contain at least one upper case letter')
        }
        else {
            error = 1;
            this.addErrorMessage('Password must contain at least one upper case letter');
            this.setState({ isUpperCase: false })
        }

        if (password.length > 6) {
            this.setState({ isLength: true })
            this.removeErrorMessage('Your password has to be at least 6 characters long.')
        }
        else {
            error = 1;
            this.addErrorMessage('Your password has to be at least 6 characters long.');
            this.setState({ isLength: false })
        }

        if (password.match(/([0-9])/)) {
            this.setState({ isDigit: true })
            this.removeErrorMessage('Your password must contain at least one digit')

        }
        else {
            error = 1;
            this.addErrorMessage('Your password must contain at least one digit');
            this.setState({ isDigit: false })
        }
        return error;

    }


    addErrorMessage(error) {
        if (errorMessage.indexOf(error) === -1) {
            errorMessage.push(error);

        }
    }

    removeErrorMessage(error) {
        if (errorMessage.includes(error) == true) {
            var index = errorMessage.indexOf(error);
            if (index > -1) {
                errorMessage.splice(index, 1);
            }
        }
    }
    onShowPassword() {
        var previousState = this.state.isSecureTextEntry;
        requestAnimationFrame(() => {
            this.setState({
                isSecureTextEntry: !previousState,
            });
        });

    }
    render() {

        return (
            <Card title={Strings.updateUsers.strPasswordTag}
                containerStyle={{ borderWidth: 0, elevation: 0}}
            >
                <Text style={headerStyle} > Your password must:</Text>
                <View style={aligSelf = "center"}>
                    <CheckBox title=' be at least 6 characters long'
                        containerStyle={instructCheckBoxConatiner}
                        uncheckedIcon='times'
                        uncheckedColor='red'
                        checkedColor='green'
                        checkedIcon='check'
                        size={10}
                        textStyle={instructStyle}
                        checked={this.state.isLength}


                    />
                    <CheckBox title=' contain at least one lower case letter'
                        containerStyle={instructCheckBoxConatiner}
                        uncheckedIcon='times'
                        checkedIcon='check'
                        checkedColor='green'
                        uncheckedColor='red'
                        checked={this.state.isLowerCase}
                        size={10}
                        textStyle={instructStyle}
                    />

                    <CheckBox title=' contain at least one upper case letter'
                        containerStyle={instructCheckBoxConatiner}
                        uncheckedIcon='times'
                        checkedIcon='check'
                        checkedColor='green'
                        uncheckedColor='red'
                        checked={this.state.isUpperCase}
                        size={10}
                        textStyle={instructStyle}
                    />

                    <CheckBox title='contain at least one digit'
                        containerStyle={instructCheckBoxConatiner}
                        uncheckedIcon='times'
                        checkedIcon='check'
                        checkedColor='green'
                        uncheckedColor='red'
                        checked={this.state.isDigit}
                        size={10}
                        textStyle={instructStyle}
                    />


                    <View style={{ padding: 30, alignContent: 'center' }}>
                        {/*Password*/}
                        <Input
                            placeholder={Strings.login.strEnterYourPwd}
                            autoCapitalize="none"
                            secureTextEntry={this.state.isSecureTextEntry}
                            leftIcon={
                                <Icon
                                    name={this.state.isSecureTextEntry ? 'eye-outline' : 'eye-off-outline'}
                                    type='material-community'
                                    size={18}
                                    color='#8B96AA'
                                    onPress={() => this.onShowPassword()}
                                />
                            }
                            underlineColorAndroid="transparent"
                            placeholderTextColor="#C5CBD9"
                            labelStyle={inputTagStyle}
                            inputContainerStyle={inputStyle}
                            label={Strings.login.strPassword.toUpperCase()}
                            onChangeText={password => { this.setState({ password }), this._onChangeText(password, this.state.confirmPassword) }}
                        />
                        {/*Confirm Password*/}
                        <Input
                            placeholder="Re-enter password"
                            autoCapitalize="none"
                            secureTextEntry={this.state.isSecureTextEntry}
                            leftIcon={
                                <Icon
                                    name={this.state.isSecureTextEntry ? 'eye-outline' : 'eye-off-outline'}
                                    type='material-community'
                                    size={18}
                                    color='#8B96AA'
                                    onPress={() => this.onShowPassword()}
                                />
                            }
                            underlineColorAndroid="transparent"
                            placeholderTextColor="#C5CBD9"
                            labelStyle={inputTagStyle}
                            inputContainerStyle={inputStyle}
                            label={"Confirm Password".toUpperCase()}
                            onChangeText={confirmPassword => { this.setState({ confirmPassword }), this._onChangeText(this.state.password, confirmPassword) }}
                        />
                    </View>
                </View>
            </Card>
        )
    }

}



