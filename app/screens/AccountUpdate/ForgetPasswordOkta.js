/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
/* eslint-disable no-return-assign */
/* eslint-disable import/newline-after-import */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  Image,
  Button
} from 'react-native';
import { Text, Card, Input } from 'react-native-elements';
import { Strings } from '../../config/Strings';
import styles from './AccountUpdateStyles';

import Constants from '../../config/Constants';
import WebService, { URL_UPDATE_PASSWORD } from '../../webservice/index';
//TOD0 -- move brand logo header to a common file
const BrandLogo = require('../../images/glimpse_brand_logo_header.png');

const { inputTagStyle, inputStyle, headerStyle, buttonWrapper } = styles;
export default class ForgetPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserName: ''
    };
  }

/*   onChangeText = UserName => {
    this.onChangeValue('UserName', UserName.trim());
  }; */

  onSubmit = () => {
    WebService.get(URL_UPDATE_PASSWORD(104, this.state.UserName, 'crap'))
    .then((response) => {
      console.log(response);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: <View />,
      headerTitle: (
        <View style={{ flex: 1 }}>
          <Image style={{ alignSelf: 'center' }} source={BrandLogo} />
        </View>
      ),
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerBackTitle: null
    };
  };

  render() {
    return (
      <KeyboardAvoidingView
        enabled={false}
        style={{ flex: 1, backgroundColor: '#fff' }}
      >
        <Card
          title={Strings.updateUsers.strForgetPassword}
          containerStyle={{ borderWidth: 0, elevation: 0 }}
        >
          <Text style={headerStyle}> {Strings.updateUsers.strUserIntro}</Text>
          <View style={(alignSelf = 'center')}>
            {/*userName*/}
            <Input
              placeholder={Strings.login.userName}
              autoCapitalize="none"
              underlineColorAndroid="transparent"
              placeholderTextColor="#C5CBD9"
              labelStyle={inputTagStyle}
              inputContainerStyle={inputStyle}
              label={Strings.login.strUserName.toUpperCase()}
              onChangeText={UserName => {
                this.setState({ UserName });
              }}
            />

            <Button
              title="Submit"
              onPress={this.onSubmit}
              clear
              titleStyle={{ color: 'white', fontFamily: 'Roboto-Bold' }}
              buttonStyle={{
                flexDirection: 'row',
                borderRadius: 25,
                height: 50
              }}
              containerStyle={buttonWrapper}
            />
          </View>
        </Card>
      </KeyboardAvoidingView>
    );
  }
}
