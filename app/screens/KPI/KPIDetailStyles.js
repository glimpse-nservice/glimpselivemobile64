export default {
  headerStyle: {
    justifyContent: "center",
    fontSize: 20,
    padding: 8,
    marginHorizontal: 16,
    marginTop: 16,
    color: "black",
    fontFamily: "Roboto-Medium"
  },
  graphView: {
    backgroundColor: "white",
    shadowRadius: 14,
    shadowOpacity: 1,
    elevation: 5,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "rgba(0, 0, 0, 0.18)",
    marginHorizontal: 16
  },
  text: {
    color: "#394263",
    fontSize: 14,
    fontFamily: "Roboto-Regular"
  },
  favIcon: {
    position: "absolute",
    elevation: 5,
    right: 0,
    padding: 0,
    width: 44,
    height: 44,
    borderRadius: 44 / 2,
    backgroundColor: "white",
    justifyContent: "center",
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "rgba(0, 0, 0, 0.18)",
    shadowRadius: 14,
    shadowOpacity: 1
  },
  growthIcon: { marginLeft: 16 },
  dividerLine: {
    height: 0.5,
    backgroundColor: "#A1A1A1"
  },
  trend: {
    color: "#394263",
    fontSize: 15,
    fontFamily: "Roboto-Bold",
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  rightValue: {
    fontSize: 14,
    color: "#394263",
    marginRight: 16,
    position: "absolute",
    right: 0
  },
  performanceIndicatorBox: {
    height: 12,
    width: 12,
    marginLeft: 14,
    borderRadius: 2
  },
  container: {
    width: 300,
    height: 300,
    overFlow: "hidden"
  }
};
