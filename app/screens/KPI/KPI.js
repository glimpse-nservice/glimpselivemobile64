import React, { Component } from "react";
import { View, FlatList, Text, Image, TouchableOpacity } from "react-native";
import Snackbar from "react-native-snackbar";
import Constants from "../../config/Constants";
import { DateLocationView, Spinner, Header } from "../../components/common";
import styles from "./KPIStyles";
import { UserData } from "../../config/UserData";
import WebService, {
  URL_KPI_LIST,
  URL_KPI_FAV,
  URL_ADD_REMOVE_FAVORITE
} from "../../webservice";
import { getStartAndEndDate, dateSet } from "../../config/Utility";
import { Strings } from "../../config/Strings";
import { Icon } from "react-native-elements";
import demoData from "../../data/staticKPIData";
import demoFav from "../../data/StaticKPIDataFav";

const imageGrowth = require("../../images/UP.png");
const imageDecline = require("../../images/down.png");

let practiceId = null;
let renderFavoriteList = false;
let practiceName = "";
let isMounted = false;
let metricName = [
  "optical",
  "financial",
  "services",
  "medical",
  "contactLens",
  "frames"
];
let tempArray = [];
let combinedResults = [];
var favCombinedResults = [];
var favResults = [];

const { strKPIList, strKPIFavorites } = Strings.kpi;
const { row, header, growthIcon, rowText } = styles;

export default class KPI extends Component {
  static navigationOptions = ({ navigation }) => {
    const headerTitle = navigation.getParam("isFavorite")
      ? strKPIFavorites
      : strKPIList;
    return {
      headerTitle: <Header headerTitle={headerTitle} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerBackTitle: null,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      loading: false,
      startDate: "",
      location: "0",
      date: dateSet[0],
      disableSelector: false
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    isMounted = true;
    renderFavoriteList = navigation.getParam("isFavorite", false);
  }

  componentWillUnmount() {
    this.isMounted = false;
    favResults = [];
    tempArray = [];
  }

  /**
   * This function fetch data from server
   * according to the location and date set by
   * the user
   * @param {*} locationId
   * @param {*} startDate
   * @param {*} endDate
   */

  getKPIListFromServer(locationId, startDate, endDate) {
    this.setState({ loading: true, disableSelector: true });
    UserData.getUserSelectedPractice()
      .then(res => {
        const userPractice = JSON.parse(res);
        practiceId = userPractice.value;
        practiceName = userPractice.label;

        return UserData.getUserLoginId();
      })
      .then(loginId => {
        if (isMounted) {
          if (renderFavoriteList) {
            WebService.get(
              URL_KPI_FAV(practiceId, locationId, loginId, startDate, endDate)
            ).then(response => {
              //get those metrics that are starred

              let starred = response.data[0].metrics.filter(metric => {
                return metric.favorite === "1";
              });
              favResults.push(starred);
              favCombinedResults = [].concat.apply([], favResults);
              this.setState({
                data: favCombinedResults,
                loading: false,
                disableSelector: false
              });
            });
          } else {
            metricName.map(name => {
              WebService.get(
                URL_KPI_LIST(
                  practiceId,
                  locationId,
                  loginId,
                  name,
                  startDate,
                  endDate
                )
              )
                .then(response => {
                  let metricInfo = response.data[0].metrics;
                  metricInfo.map(item => {
                    tempArray.push(item);
                  });

                  combinedResults = [].concat.apply([], tempArray);
                  //we need to show favorites on top
                  const sortedArr = combinedResults.sort((metric1, metric2) => {
                    return metric2.favorite - metric1.favorite;
                  });
                  this.setState({
                    data: sortedArr,
                    loading: false,
                    disableSelector: false
                  });
                })
                .catch(error => {
                  Snackbar.show({
                    title: error.message,
                    duration: Snackbar.LENGTH_SHORT
                  });
                  this.setState({ loading: false, disableSelector: false });
                });
            });
          }
        }
      })
      .catch(() => {
        Snackbar.show({
          title: "Unable to fetch data from server. Please try again",
          duration: Snackbar.LENGTH_SHORT
        });
        this.setState({ loading: false, disableSelector: false });
      });
  }
  /* 
    multiDimensionalUnique(arr) {
      var uniques = [];
      var itemsFound = {};
      for (var i = 0, l = arr.length; i < l; i++) {
        var stringified = JSON.stringify(arr[i].key);
        if (itemsFound[stringified]) { continue; }
        uniques.push(arr[i]);
        itemsFound[stringified] = true;
      }
      return uniques;
    } */
  /**
   * This function is called whenever user selects a date
   * @param { new date that is selected by the user } dateObj
   */
  onDateOrLocationSet(dateObj, newLocation) {
    const date = getStartAndEndDate(dateObj.value);

    if (dateObj !== this.state.date || newLocation !== this.state.location) {
      this.setState(
        {
          date: dateObj,
          location: newLocation
        },
        () => {
          this.setState({ loading: true, disableSelector: true });
          favResults = [];
          tempArray = [];
          this.getKPIListFromServer(
            this.state.location,
            date.startDate,
            date.endDate
          );
        }
      );
    }
  }

  setStarIcon(item) {
    return (
      <Icon
        raised
        name={item.favorite === "1" ? "star" : "star-o"}
        type="font-awesome"
        size={24}
        color="#ffdc63"
        onPress={() => this.addRemoveFavorite(item.key, item.favorite)}
      />
    );
  }

  getAllStartedItems() {
    let temp = combinedResults.filter(items => {
      return items.favorite === "1";
    });
    return temp.length;
  }

  addRemoveFavorite(metric, favorite) {
    let starredList = this.getAllStartedItems();
    if (starredList >= 10 && favorite == "0") {
      alert("You can only add 10 favorites");
    } else {
      UserData.getUserLoginId()
        .then(res => {
          const apiURL = URL_ADD_REMOVE_FAVORITE(
            res,
            metric,
            Number.parseInt(favorite, 10)
          );
          WebService.get(apiURL)
            .then(() => {
              isFavorite = !favorite;
              this.setState({ loading: false });
              this.reloadKPIList(metric);
            })
            .catch(error => {
              Snackbar.show({
                title: error.message,
                duration: Snackbar.LENGTH_SHORT
              });
              this.setState({ loading: false });
            });
        })
        .catch(() => {
          Snackbar.show({
            title: "Unable to fetch data from server. Please try again",
            duration: Snackbar.LENGTH_SHORT
          });
          this.setState({ loading: false });
        });
    }
  }
  reloadKPIList(item) {
    const data = this.state.data;
    let indexToRemove = null;
    data.every((metricItem, index) => {
      if (metricItem.key === item) {
        if (this.props.navigation.getParam("isFavorite")) {
          indexToRemove = index;
          return false;
        }
        //setting metric favorite property here
        metricItem.favorite = metricItem.favorite === "1" ? "0" : "1";
        return false;
      }
      return true;
    });

    if (indexToRemove != null) {
      data.splice(indexToRemove, 1);
      this.setState({ data });
    } else {
      const sortedArr = data.sort(
        (metric1, metric2) => metric2.favorite - metric1.favorite
      );
      this.setState({ data: sortedArr });
    }
  }

  renderMainView() {
    if (this.state.loading) {
      return <Spinner />;
    }

    if (this.state.data != null) {
      if (this.state.data.length === 0) {
        return (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Text
              style={{
                color: "#394263",
                fontSize: 14,
                fontFamily: "Roboto-Medium"
              }}
            >
              No favorites
            </Text>
          </View>
        );
      }
    }

    return (
      <View style={{ flex: 1 }}>
        <Text style={header}>{practiceName}</Text>
        <FlatList
          style={{ paddingHorizontal: 8, marginBottom: 8 }}
          data={this.state.data}
          renderItem={({ item }) => {
            return (
              <View style={row}>
                <TouchableOpacity
                  style={{ flexDirection: "row", justifyContent: "center" }}
                  onPress={() => {
                    this.props.navigation.navigate("KPIDetail", {
                      title: item.label,
                      metric: item.key,
                      isFavorite: item.favorite,
                      isFromFavorite: this.props.navigation.getParam(
                        "isFavorite"
                      ),
                      slocation: this.state.location,
                      sDate: this.state.date,
                      starredLength: this.getAllStartedItems(),
                      forceReload: () => {
                        this.reloadKPIList(item.key);
                      }
                    });
                  }}
                >
                  <Image
                    style={growthIcon}
                    source={item.growth > 0 ? imageGrowth : imageDecline}
                  />
                  <Text style={rowText}>{item.label}</Text>
                </TouchableOpacity>
                {this.setStarIcon(item)}
              </View>
            );
          }}
          keyExtractor={item => item.key}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <DateLocationView
          disabled={this.state.disableSelector}
          onDateOrLocationSet={this.onDateOrLocationSet.bind(this)}
          labelLoc={this.state.location}
          labelDate={this.state.date}
        />

        {this.renderMainView()}
      </View>
    );
  }
}
