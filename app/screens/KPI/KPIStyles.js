

export default {
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    justifyContent: 'center',
    fontSize: 20,
    padding: 8,
    marginHorizontal: 16,
    marginTop: 16,
    color: 'black',
    fontFamily: 'Roboto-Medium'
  },
  row: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 4,
    paddingRight: 15,
    borderRadius: 5,
    shadowRadius: 14,
    shadowOpacity: 1,
    elevation: 3,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0, 0, 0, 0.14)'
  },
  growthIcon: {
    marginHorizontal: 5,
    alignSelf: 'center',
    padding: 2

  },

  rowText: {
    padding: 10,
    color: '#394263',
    fontSize: 18,
    flexWrap: 'wrap',
    flex: 0.8

  }
}
