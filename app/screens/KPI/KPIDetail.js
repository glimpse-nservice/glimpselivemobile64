/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable eqeqeq */
/* eslint-disable no-unused-vars */
/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable no-undef */
import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  Dimensions,
  Image,
  BackHandler,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import {
  AreaChart,
  Grid,
  YAxis,
  XAxis,
  G,
  Line
} from "react-native-svg-charts";
import * as shape from "d3-shape";

import Snackbar from "react-native-snackbar";
import Constants from "../../config/Constants";
import { DateLocationView, Spinner, Header } from "../../components/common";
import styles from "./KPIDetailStyles";
import { UserData } from "../../config/UserData";
import WebService, {
  URL_ADD_REMOVE_FAVORITE,
  URL_KPI_DETAIL_LIST
} from "../../webservice";
import {
  getStartAndEndDate,
  dateSet,
  numberWithCommas,
  isPortrait,
  formatNumber
} from "../../config/Utility";
import { Icon } from "react-native-elements";
import demoData from "../../data/staticKPIData";
import * as scale from "d3-scale";

const imgDown = require("../../images/down.png");
const imgUp = require("../../images/UP.png");
const imgZeroGrowth = require("../../images/growthZero.png");

let practiceId = null;
let switchCheck = 0;
let isFavorite = null;
let growthIcon = imgDown;
let practiceName = "";
let isMounted = false;
let selectedDateIndex = 0;
const currentColor = "rgb(153, 204, 255)";
const prevColor = "rgba(204, 204,204, 0.5)";
let locationType = null;
let filteredObj = [];
const { height } = Dimensions.get("window");

export default class KPIDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      dataLoading: false,
      loading: false,
      disableSelector: false,
      startDate: "",
      location: "0",
      date: dateSet[0],
      orientation: isPortrait() ? "portrait" : "landscape"
    };
    this.onBackPressed = this.onBackPressed.bind(this);

    // Event Listener for orientation changes
    Dimensions.addEventListener("change", () => {
      this.setState({
        orientation: isPortrait() ? "portrait" : "landscape"
      });
    });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle={navigation.getParam("title")} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  componentDidMount() {
    isMounted = true;
    isFavorite = this.props.navigation.getParam("isFavorite") === "1";
    BackHandler.addEventListener("hardwareBackPress", this.onBackPressed);
  }

  componentWillUnmount() {
    isMounted = false;
    switchCheck = 0;
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPressed);
  }

  /**
   * This function is being called when
   * back button is being pressed
   */
  onBackPressed() {
    if (this.state.dataLoading) {
      return true;
    }
    return false;
  }

  onDateOrLocationSet(dateObj, newLocation) {
    let forwardedDate = this.props.navigation.getParam("sDate");
    let forwardedLocation = this.props.navigation.getParam("slocation");
    if (switchCheck === 0) {
      dateObj = forwardedDate;
      newLocation = forwardedLocation;
      switchCheck = 1;
    }

    const date = getStartAndEndDate(dateObj.value);
    selectedDateIndex = dateObj.value;
    this.setState(
      {
        date: dateObj,
        location: newLocation,
        dataLoading: true,
        disableSelector: true
      },
      () => {
        this.getKpiDetailFromServer(newLocation, date.startDate, date.endDate);
      }
    );
  }

  /**
   * This function gets the data from server according to
   * the location, date and metric selected
   * @param {location selected} locationId
   * @param {start date selected} startDate
   * @param {end date selected} endDate
   */
  getKpiDetailFromServer(locationId, startDate, endDate) {
    const metricKey = this.props.navigation.getParam("metric");

    this.setState({ dataLoading: true, disableSelector: true });
    UserData.getUserSelectedPractice().then(res => {
      const userPractice = JSON.parse(res);
      practiceId = userPractice.value;
      practiceName = userPractice.label;

      WebService.get(
        URL_KPI_DETAIL_LIST(
          practiceId,
          locationId,
          startDate,
          endDate,
          metricKey
        )
      )
        .then(response => {
          if (isMounted && response !== null) {
            this.setState({
              data: response.data[0],
              dataLoading: false,
              disableSelector: false
            });
          }
        })
        .catch(error => {
          Snackbar.show({
            title: error.message,
            duration: Snackbar.LENGTH_SHORT
          });
          this.setState({ dataLoading: false, disableSelector: false });
        });
    });
  }

  render() {
    return (
      <View style={{ backgroundColor: "white", flex: 1 }}>
        {/* Date and Location View */}
        <DateLocationView
          disabled={this.state.disableSelector}
          onDateOrLocationSet={this.onDateOrLocationSet.bind(this)}
          labelLoc={this.state.location}
          labelDate={this.state.date}
        />

        {/*Data View*/}
        {this.renderDataOnScreen()}
      </View>
    );
  }

  /**
   * This function renders views on screen
   */
  renderDataOnScreen() {
    let chartHeight = 0;
    let chartWidth = 0;

    if (this.state.orientation === "portrait") {
      chartHeight = Dimensions.get("window").height * 0.3;
      chartWidth = Dimensions.get("window").width - 40;
    } else {
      chartHeight = Dimensions.get("window").height * 0.6;
      chartWidth = Dimensions.get("window").width - 40;
    }

    const {
      headerStyle,
      graphView,
      growthIconStyle,
      text,
      favIcon,
      dividerLine,
      performanceIndicatorBox,
      rightValue
    } = styles;

    if (!this.state.dataLoading) {
      if (this.state.data !== null) {
        const metricData = this.state.data.metrics[0];
        const growthPct = metricData.growth;
        const title = metricData.label;
        if (growthPct === 0) {
          growthIcon = imgZeroGrowth;
        } else {
          growthIcon = growthPct > 0 ? imgUp : imgDown;
        }

        const currentData = metricData.data;
        const previousData = metricData.prev;

        let cData = [];
        let preData = [];
        let cDates = [1, 2];
        let clabel = [];

        /*    function funcName(key) {
          var item = previousData.slice(key, key + 1);
          var returnItem;
          item.map((x, y) => {
            returnItem = x.data;
          });
          return returnItem;
        } */
        previousData.map(pProp => {
          preData.push(pProp.data);
        });

        let clength = Object.keys(currentData).length;

        currentData.map((prop, key) => {
          cData.push(prop.data);
          if (key == 0) {
            clabel.push(prop.label);
          }
          if (key == clength - 1) {
            clabel.push(prop.label);
          }

          // previous: funcName(key)
        });

        let totalCurrent = null;
        let totalPrev = null;

        switch (metricData.dataType) {
          case "currency":
            totalCurrent = `$${numberWithCommas(
              formatNumber(metricData.total)
            )}`;
            totalPrev = `$${numberWithCommas(
              formatNumber(metricData.prevTotal)
            )}`;

            break;
          case "pct":
            totalCurrent = `${metricData.total.toFixed(1)}%`;
            totalPrev = `${metricData.prevTotal.toFixed(1)}%`;
            break;
          case "units":
            totalCurrent = `${metricData.total}`;
            totalPrev = `${metricData.prevTotal}`;
            break;
          default:
        }

        let minPrev = 0;
        let maxPrev = 0;
        let minCurrent = 0;
        let maxCurrent = 0;
        const YAxisData = cData.concat(preData);

        const axesSvg = { fontSize: 10, fill: "grey" };
        const verticalContentInset = { top: 5, bottom: 5 };

        return (
          <ScrollView>
            <View style={{ marginBottom: 20 }}>
              {/* Header Label */}
              <Text style={headerStyle}>{practiceName}</Text>
              {/* Graph View */}
              <View style={[graphView, { height: chartHeight * 2.1 }]}>
                {/* Growth Value View */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginLeft: 15,
                    marginRight: 15
                  }}
                >
                  <Image style={growthIconStyle} source={growthIcon} />
                  <Text
                    style={[
                      text,
                      { paddingVertical: 16, paddingHorizontal: 8 }
                    ]}
                  >
                    {growthPct.toFixed(1)}% Growth
                  </Text>
                  <Icon
                    name={isFavorite ? "star" : "star-o"}
                    type="font-awesome"
                    size={25}
                    color="#ffdc63"
                    containerStyle={favIcon}
                    onPress={() => this.addRemoveFavorite()}
                  />
                </View>
                {/* Divider Line */}
                <View style={dividerLine} />
                {/* Trend */}
                <Text style={styles.trend}>{`Trends for ${title}`}</Text>
                {/* Graph/Chart */}

                <View style={{ marginBottom: 20, marginTop: 20 }}>
                  <YAxis
                    style={{
                      position: "absolute",
                      height: chartHeight,
                      marginLeft: 5
                    }}
                    contentInset={verticalContentInset}
                    svg={axesSvg}
                    data={YAxisData}
                    formatLabel={value =>
                      value >= 1000
                        ? `${(value / 1000).toFixed(0)}k`
                        : value.toFixed(0)
                    }
                    min={0}
                  />
                  <AreaChart
                    style={{
                      position: "absolute",
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 0,
                      height: chartHeight
                    }}
                    data={cData}
                    svg={{
                      fill: "rgba(153, 204, 255, 0.5)",
                      stroke: "rgb(153, 204, 255)"
                    }}
                    contentInset={{ left: 30, right: 10 }}
                    curve={shape.curveNatural}
                  >
                    <Grid />
                  </AreaChart>
                  <AreaChart
                    style={{
                      position: "absolute",
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 0,
                      height: chartHeight
                    }}
                    data={preData}
                    svg={{
                      fill: "rgba(204, 204,204, 0.5)",
                      stroke: "rgb(204, 204,204)"
                    }}
                    contentInset={{ left: 30, right: 10 }}
                    curve={shape.curveNatural}
                  />

                  <XAxis
                    data={cDates}
                    svg={{ fontSize: 10, fill: "gray" }}
                    contentInset={{ left: 30, right: 30, bottom: 0, top: 0 }}
                    formatLabel={index => clabel[index]}
                    style={{
                      position: "relative",
                      bottom: -chartHeight,
                      padding: 10
                    }}
                  />
                </View>

                <View
                  style={{
                    bottom: 0,
                    right: 0,
                    left: 0,
                    position: "absolute"
                  }}
                >
                  {/* My Performance */}
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={[
                        performanceIndicatorBox,
                        { backgroundColor: currentColor }
                      ]}
                    />
                    <Text style={[{ padding: 8 }, text]}>My Performance</Text>
                    <Text style={rightValue}>{totalCurrent}</Text>
                  </View>
                  {/* Previous Time Frame */}
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginBottom: 8
                    }}
                  >
                    <View
                      style={[
                        performanceIndicatorBox,
                        { backgroundColor: prevColor }
                      ]}
                    />
                    <Text style={[{ padding: 8 }, text]}>
                      Previous Time Frame
                    </Text>
                    <Text style={rightValue}>{totalPrev}</Text>
                  </View>
                </View>
              </View>
              {/* ADD TO KPI WATCHLIST */}
            </View>
          </ScrollView>
        );
      }
    }
    return <Spinner />;
  }

  /**
   * This function computes the points
   * that needs to be
   * put on left axis of graph
   */
  computeLeftAxisData(min, max) {
    const leftAxisData = [];

    leftAxisData.push(Number(min.toFixed(2)));
    const b = (min + max) / 2;
    const a = (b + min) / 2;
    const c = (b + max) / 2;

    leftAxisData.push(Number(a.toFixed(2)));
    leftAxisData.push(Number(b.toFixed(2)));
    leftAxisData.push(Number(c.toFixed(2)));
    leftAxisData.push(Number(max.toFixed(2)));

    return leftAxisData;
  }

  /**
   * This function computes the values
   * that needs to be
   * displayed for the points on
   * left axis of graph
   */
  computeLeftAxisDataToShow(min, max) {
    const leftAxisData = [];

    leftAxisData.push(
      Math.abs(min) / 1000 > 1 ? `${(min / 1000).toFixed(0)}k` : min.toFixed(2)
    );
    const b = (min + max) / 2;
    const a = (b + min) / 2;
    const c = (b + max) / 2;

    leftAxisData.push(
      Math.abs(a) / 1000 > 1 ? `${(a / 1000).toFixed(0)}k` : a.toFixed(2)
    );
    leftAxisData.push(
      Math.abs(b) / 1000 > 1 ? `${(b / 1000).toFixed(0)}k` : b.toFixed(2)
    );
    leftAxisData.push(
      Math.abs(c) / 1000 > 1 ? `${(c / 1000).toFixed(0)}k` : c.toFixed(2)
    );
    leftAxisData.push(
      Math.abs(max) / 1000 > 1 ? `${(max / 1000).toFixed(0)}k` : max.toFixed(2)
    );

    return leftAxisData;
  }

  /*
  Function to add/remove favorite
  */
  addRemoveFavorite() {
    const metric = this.props.navigation.getParam("metric");
    const reloadKPIList = this.props.navigation.getParam("forceReload");
    const favLength = this.props.navigation.getParam("starredLength");
    if (favLength >= 10 && isFavorite != "1") {
      alert("You can only add 10 favorites");
    } else {
      this.setState({ loading: true });

      UserData.getUserLoginId()
        .then(res => {
          const apiURL = URL_ADD_REMOVE_FAVORITE(res, metric, isFavorite);

          WebService.get(apiURL)
            .then(() => {
              isFavorite = !isFavorite;
              this.setState({ loading: false });
              reloadKPIList(metric);
            })
            .catch(error => {
              Snackbar.show({
                title: error.message,
                duration: Snackbar.LENGTH_SHORT
              });
              this.setState({ loading: false });
            });
        })
        .catch(() => {
          Snackbar.show({
            title: "Unable to fetch data from server. Please try again",
            duration: Snackbar.LENGTH_SHORT
          });
          this.setState({ loading: false });
        });
    }
  }
}
