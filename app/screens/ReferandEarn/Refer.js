import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import Constants from '../../config/Constants';
import { Strings } from '../../config/Strings';
import styles from './ReferStyles';
import { Header } from '../../components/common/index';
import { openURL } from '../../config/Utility';
import loginStyles from '../Login/LoginStyle';

const referImage = require('../../images/refer.png');

// const shareImage = '';
const forwardImage = '';

export default class Refer extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle="Refer & Earn" />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerTitleStyle: Constants.headerTitleStyle,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  /**
   * This function is called when the share button is clicked.
   * When clicked, it opens the share sheet with the list of
   * apps available to share.
   */
  onShareBtnClicked() {
    // Share.share(
    //   {
    //     message: 'Check out Glimpse App. \n https://www.glimpselive.com/website/splash.php',
    //     title: 'Glimpse Live'
    //   },
    //   {
    //     // Android only:
    //     dialogTitle: 'Invite your friends'
    //   }
    // );
    this.props.navigation.navigate('ReferForm');
  }

  render() {
    return (
      <ScrollView>
        <View>
          {/*Header Card*/}
          <View style={styles.headerCardContainerStyle}>
            <Image style={{ margin: 10 }} source={referImage} />

            <Text numberOfLines={2} style={styles.referTitleTextStyle}>
              {Strings.refer.strReferTitle}
            </Text>

            <Text numberOfLines={4} style={styles.referDescTextStyle}>
              {Strings.refer.strReferDesc}
            </Text>
          </View>

          {/*Share Button*/}
          <TouchableOpacity style={[loginStyles.loginButtonStyle, { marginBottom: 10 }]} onPress={this.onShareBtnClicked.bind(this)}>
            <Text style={{ color: 'white', fontFamily: 'Roboto-Bold' }}>{Strings.referForm.strRefer.toUpperCase()}</Text>
          </TouchableOpacity>

          {/*Terms and Conditions*/}
          <TouchableOpacity onPress={() => openURL(Constants.urlTerms)}>
            <View style={styles.termsAndConditionsContainerStyle}>
              <Text style={styles.termsAndConditionsTextStyle}>Terms and Conditions</Text>
              <Text
                style={{
                  fontFamily: 'icomoon',
                  fontSize: 15
                }}
              >
                {forwardImage}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
