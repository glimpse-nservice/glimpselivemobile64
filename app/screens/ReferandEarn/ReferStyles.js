export default {
  headerCardContainerStyle: {
    paddingBottom: 15,
    paddingTop: 20,
    marginBottom: 10,
    elevation: 3,
    shadowOpacity: 1,
    shadowOffset: { width: 4, height: 4 },
    shadowColor: 'rgba(0.14, 0.14, 0.17, 0.07)',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  referTitleTextStyle: {
    margin: 5,
    fontSize: 18,
    color: '#394263',
    textAlign: 'center',
    fontFamily: 'Roboto-Medium',
    fontWeight: '200'
  },
  referDescTextStyle: {
    margin: 5,
    fontSize: 14,
    color: '#c3c3c3',
    textAlign: 'center',
    fontFamily: 'Roboto-Regular'
  },
  shareButtonStyle: {
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 20,
    elevation: 3,
    shadowOpacity: 1,
    shadowOffset: { width: 4, height: 4 },
    shadowColor: 'rgba(0.14, 0.14, 0.17, 0.07)',
    backgroundColor: 'white'
  },
  shareIconStyle: {
    fontSize: 24,
    color: '#25AAE1',
    textAlign: 'center',
    padding: 10,
    margin: 10,
    fontFamily: 'icomoon'
  },
  shareTextStyle: {
    fontSize: 18,
    color: '#394263',
    textAlign: 'center',
    margin: 10,
    fontFamily: 'Roboto-Medium',
    fontWeight: '200'
  },
  termsAndConditionsContainerStyle: {
    backgroundColor: 'white',
    flexDirection: 'row',
    margin: 20,
    padding: 20,
    elevation: 3,
    shadowOpacity: 1,
    shadowOffset: { width: 4, height: 4 },
    shadowColor: 'rgba(0.14, 0.14, 0.17, 0.07)',
    justifyContent: 'space-between'
  },
  termsAndConditionsTextStyle: {
    fontSize: 15,
    fontFamily: 'Roboto-Medium',
    fontWeight: '100',
    color: '#394263'
  }
};
