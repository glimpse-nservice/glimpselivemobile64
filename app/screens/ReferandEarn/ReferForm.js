import React from 'react';
import { View, Text, KeyboardAvoidingView, ScrollView, TextInput, TouchableOpacity, Keyboard, Linking } from 'react-native';
import styles from '../Login/LoginStyle';
import { Strings } from '../../config/Strings';
import Constants from '../../config/Constants';
import { Spinner, Header } from '../../components/common';
import { UserData } from '../../config/UserData';

const { screenContainerStyle, inputTagStyle, inputStyle, loginButtonStyle, errorTextStyle } = styles;
const { strEmail, strEmailError } = Strings.submitSupportTicket;
const { strTitle, strRefer, strEnterReferralEmail, strEnterReferralName, strReferralNameError, strReferralName } = Strings.referForm;

class ReferForm extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle={strTitle} />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  constructor() {
    super();
    this.state = { email: '', referralName: '', loading: false };
  }

  /**
   * This function is called when refer button is clicked.
   * it checks if user has entered information or not, if not shows
   * error to the user. If yes, then sends email to
   * glimpse support.
   */
  onBtnClicked() {
    Keyboard.dismiss();

    if (this.state.email === undefined || this.state.email === '') {
      this.setState({ error: strEmailError, loading: false });
      return;
    }

    if (this.state.referralName === undefined || this.state.referralName === '') {
      this.setState({ error: strReferralNameError, loading: false });
      return;
    }


    UserData.getUserName()
      .then(username => {
        if (username !== null) {
          const emailBody = `Referring client name:- \n${username}\n\nReferral Email:- ${this.state.email}\n\nReferral Name:- ${this.state.referralName}`;
          Linking.openURL(`mailto:${Strings.contactUs.strSalesEmail}?subject=${Constants.referEmailSubject}&body=${emailBody}`);
        }
      });
  }

  /**
   * This function renders the refer button or
   * loader.
   */
  renderEmailButton() {
    if (this.state.loading) {
      return (
        <View style={{ marginTop: 20 }}>
          <Spinner flag="login" />
        </View>
      );
    }
    return (
      <TouchableOpacity style={loginButtonStyle} onPress={() => this.onBtnClicked()}>
        <Text style={{ color: 'white', fontFamily: 'Roboto-Bold' }}>{strRefer.toUpperCase()}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <KeyboardAvoidingView enabled style={{ flex: 1 }}>
        <View style={screenContainerStyle}>
          <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="always" contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
            {/*Submit support ticket Form*/}
            <View style={{ marginTop: 20 }}>
              {/*Email*/}
              <Text style={inputTagStyle}>{strEmail.toUpperCase()}</Text>
              <TextInput
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                placeholder={strEnterReferralEmail}
                placeholderTextColor="#C5CBD9"
                keyboardType={'email-address'}
                style={inputStyle}
                label={strEnterReferralEmail}
                value={this.state.email}
                onChangeText={email => {
                  this.setState({ email, error: '' });
                }}
              />

              {/* Referral Name */}
              <Text style={inputTagStyle}>{strReferralName.toUpperCase()}</Text>
              <TextInput
                autoCapitalize="none"
                underlineColorAndroid="transparent"
                placeholder={strEnterReferralName}
                placeholderTextColor="#C5CBD9"
                style={inputStyle}
                label={strEnterReferralEmail}
                value={this.state.referralName}
                onChangeText={referralName => {
                  this.setState({ referralName, error: '' });
                }}
              />

              {/*Error message*/}
              <Text style={errorTextStyle}>{this.state.error}</Text>
              {/*Refer button*/}
              {this.renderEmailButton()}
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default ReferForm;
