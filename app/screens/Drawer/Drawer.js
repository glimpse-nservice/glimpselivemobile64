import PropTypes from 'prop-types';
import React, { Component } from 'react';
import VersionNumber from 'react-native-version-number';
import { ScrollView, Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './DrawerStyles';
import { Strings } from '../../config/Strings';
import { onLogOut } from '../../helper/auth';
import { UserData } from '../../config/UserData';
const imageIcon = require('../../images/icon.png');
const { textStyle, topViewStyle, topTextStyle, disabledTextStyle } = styles;
const { strGlimpse, strKPIsList, strSubmitSupportTicket, strKpiFavorites, strDailySnapshot, strOpportunities,
  strSwot, strContactUs, strSignOut } = Strings.drawer;

class Drawer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showResults: false,
      loginId: 0,
      practiceId: 0,
      flag: false,
      selectedPractice: UserData.getUserSelectedPractice().then(res => {
        if (JSON.parse(res) != null) {
          if (JSON.parse(res).accountTypeId != undefined) {
            if (JSON.parse(res).accountTypeId == 2) {
              global.IsSelectedLite = true;
            }
            else {
              global.IsSelectedLite = false;
            }
          }
        }
      }).catch(message => alert(message))

    }
  }
  navigateToScreen = () => {
    this.props.navigation.navigate('LoggedOut');
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={topViewStyle}>
            <Image source={imageIcon} />
            <Text style={topTextStyle}>{strGlimpse}</Text>
          </View>
          {/* KPI List */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('KPI')}
          >
            <Text style={textStyle}>{strKPIsList}</Text>
          </TouchableOpacity>
          {/* KPI Favorites */}
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('KPI', {
                isFavorite: true
              })
            }
          >
            <Text style={textStyle}>{strKpiFavorites}</Text>
          </TouchableOpacity>

          {/*Daily Snapshot */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('DailySnapShot')}
          >
            <Text style={textStyle}>{strDailySnapshot}</Text>
          </TouchableOpacity>

          {/* Opportunities */}
          <TouchableOpacity disabled={global.IsSelectedLite}
            onPress={() => this.props.navigation.navigate('Opportunities')} >
            <Text style={global.IsSelectedLite ? disabledTextStyle : textStyle}>{strOpportunities}</Text>
          </TouchableOpacity>

          {/* SWOT */}
          <TouchableOpacity disabled={global.IsSelectedLite}
            onPress={() => this.props.navigation.navigate('Swot')}
          >
            <Text style={global.IsSelectedLite ? disabledTextStyle : textStyle}>{strSwot}</Text>
          </TouchableOpacity>

          {/* Submit Support Ticket */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SupportTicket')}
          >
            <Text style={textStyle}>{strSubmitSupportTicket}</Text>
          </TouchableOpacity>

          {/* Contact Us */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ContactUs')}
          >
            <Text style={textStyle}>{strContactUs}</Text>
          </TouchableOpacity>
          {/* Sign Out */}
          <TouchableOpacity
            onPress={() => {
              onLogOut().then(() => {
                UserData.clearAll()
                  .then(() => {
                    this.navigateToScreen();
                  })
                  .catch();
              });
            }}
          >
            <Text style={textStyle}>{strSignOut}</Text>
          </TouchableOpacity>
        </ScrollView>

        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text>App Version:- {VersionNumber.appVersion}</Text>
          <Text>Build Number:- {VersionNumber.buildVersion}</Text>
        </View>
      </View>
    );
  }
}

Drawer.propTypes = {
  navigation: PropTypes.object
};

export default Drawer;