export default {
  textStyle: {
    padding: 16,
    color: '#394263',
    fontFamily: 'Roboto-Regular',
    fontSize: 16
  },
  topViewStyle: {
    flexDirection: 'row',
    marginTop: 80,
    marginLeft: 16,
    marginBottom: 40,
    alignItems: 'center'
  },
  topTextStyle: {
    marginLeft: 16,
    fontSize: 20,
    fontFamily: 'Roboto-Regular',
    color: '#394263',
    fontWeight: '500'
  },
  disabledTextStyle: {
    padding: 16,
    color: "#999",
    fontFamily: 'Roboto-Regular',
    fontSize: 16
  }
};
