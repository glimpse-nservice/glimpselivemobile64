import React, { Component } from "react";
import Snackbar from "react-native-snackbar";
import { View, Dimensions, TouchableOpacity, Text } from "react-native";
import { IndicatorViewPager, PagerTitleIndicator } from "rn-viewpager";
import OpportunityTab from "./OpportunityTab";
import Constants from "../../config/Constants";
import WebService, { URL_OPPORTUNITIES } from "../../webservice";
import { LocationView, Header, Spinner } from "../../components/common/index";
import { getStartAndEndDate, dateSet } from "../../config/Utility";
import { UserData } from "../../config/UserData";
import demoData from "../../data/OppData";

let viewPager;
let isMounted = false;
let practiceId = "";
let practiceName = "";

export default class Opportunities extends Component {
  constructor() {
    super();

    this.state = {
      opticalData: null,
      contactLensData: null,
      officeVisitsData: null,
      clinicalData: null,
      loading: true,
      startDate: "",
      location: "0",
      date: dateSet[5],
      disableSelector: false
    };
  }

  componentDidMount() {
    isMounted = true;
  }

  componentWillUnmount() {
    isMounted = false;
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <Header headerTitle="Opportunities" />,
      headerStyle: Constants.headerStyle,
      headerTintColor: Constants.headerTintColor,
      /*
      setting a custom back button here to solve the screen retention
      when going back home from any tab other than 1
      */
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            if (viewPager !== null && viewPager !== undefined) {
              viewPager.setPageWithoutAnimation(0);
            } //go to first page
            navigation.goBack();
          }}
        >
          <Text style={styles.backButton}></Text>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Text style={Constants.sideMenuStyle}></Text>
        </TouchableOpacity>
      )
    };
  };

  // Top title indicators
  renderTitleIndicator() {
    const titles = ["Optical", "Contact Lens", "Clinical", "Office Visits"];
    const itemWidth = { width: Dimensions.get("window").width / titles.length };

    return (
      <PagerTitleIndicator
        titles={titles}
        style={styles.indicatorContainer}
        selectedBorderStyle={styles.indicatorBorder}
        itemTextStyle={[styles.indicatorText, itemWidth]}
        selectedItemTextStyle={[styles.indicatorSelectedText, itemWidth]}
        trackScroll
      />
    );
  }

  /**
   * This function is called whenever user changes location
   */
  onLocationSet(newLocation) {
    let date = getStartAndEndDate(this.state.date.value);
    if (newLocation !== this.state.location) {
      this.setState(
        {
          location: newLocation
        },
        () => {
          this.setState({ loading: true, disableSelector: true });

          this.getDataFromServer(
            this.state.location,
            date.startDate,
            date.endDate
          );
        }
      );
    }
  }

  getApiRequest(apiUrl) {
    return WebService.get(apiUrl);
  }

  /**
   * This function fetch data from server
   * according to the location and date set by
   * the user
   */
  getDataFromServer(location, startDate, endDate) {
    this.setState({ loading: true, disableSelector: true });

    UserData.getUserSelectedPractice()
      .then(res => {
        if (res !== null) {
          const userPractice = JSON.parse(res);
          practiceId = userPractice.value;
          practiceName = userPractice.label;

          const apiURL1 = URL_OPPORTUNITIES(
            "optical",
            practiceId,
            location,
            startDate,
            endDate
          );
          const apiURL2 = URL_OPPORTUNITIES(
            "contactLens",
            practiceId,
            location,
            startDate,
            endDate
          );
          const apiURL3 = URL_OPPORTUNITIES(
            "Clinical",
            practiceId,
            location,
            startDate,
            endDate
          );
          const apiURL4 = URL_OPPORTUNITIES(
            "officeVisits",
            practiceId,
            location,
            startDate,
            endDate
          );

          Promise.all([
            this.getApiRequest(apiURL1),
            this.getApiRequest(apiURL2),
            this.getApiRequest(apiURL3),
            this.getApiRequest(apiURL4)
          ])
            .then(([res1, res2, res3, res4]) => {
              if (isMounted) {
                //console.log(res1, res2, res3, res4);
                this.setState({
                  opticalData: res1.data,
                  contactLensData: res2.data,
                  clinicalData: res3.data,
                  officeVisitsData: res4.data,
                  loading: false,
                  disableSelector: false
                });
              }
              console.log(this.state.opticalData);
            })
            .catch(error => {
              Snackbar.show({
                title: error.message,
                duration: Snackbar.LENGTH_SHORT
              });
              this.setState({ loading: false, disableSelector: false });
            });
        }
      })
      .catch(() => {
        Snackbar.show({
          title: "Unable to fetch data from server. Please try again",
          duration: Snackbar.LENGTH_SHORT
        });
        this.setState({ loading: false, disableSelector: false });
      });
  }

  render() {
    return (
      <View style={{ backgroundColor: "white", flex: 1 }}>
        <LocationView
          disabled={this.state.disableSelector}
          onLocationSet={this.onLocationSet.bind(this)}
          labelLoc={this.state.location}
        />
        {/*This function renders data on screen i.e. either spinner or tabs*/}
        {this.renderDataOnScreen()}
      </View>
    );
  }

  renderDataOnScreen() {
    if (!this.state.loading) {
      return (
        <IndicatorViewPager
          style={{
            flex: 1,
            marginTop: 16,
            backgroundColor: "white",
            flexDirection: "column-reverse"
          }}
          indicator={this.renderTitleIndicator()}
          ref={viewpager => {
            viewPager = viewpager;
          }}
        >
          {/* Tabs */}
          <View>
            <OpportunityTab
              data={this.state.opticalData}
              practiceName={practiceName}
            />
          </View>
          <View>
            <OpportunityTab
              data={this.state.contactLensData}
              practiceName={practiceName}
            />
          </View>
          <View>
            <OpportunityTab
              data={this.state.clinicalData}
              practiceName={practiceName}
            />
          </View>
          <View>
            <OpportunityTab
              data={this.state.officeVisitsData}
              practiceName={practiceName}
            />
          </View>
        </IndicatorViewPager>
      );
    }
    return <Spinner />;
  }
}

const styles = {
  pageContainer: {
    flex: 1,
    backgroundColor: "white",
    padding: 20
  },
  indicatorContainer: {
    height: 50,
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: "white"
  },
  pagerStyle: {
    marginTop: 50
  },
  indicatorText: {
    color: "#C5CBD9",
    textAlign: "center"
  },
  indicatorSelectedText: {
    color: "#25AAE1",
    textAlign: "center"
  },
  indicatorBorder: {
    backgroundColor: "#25AAE1"
  },
  backButton: {
    fontFamily: "icomoon",
    fontSize: 20,
    color: "white",
    paddingRight: 16,
    paddingLeft: 8,
    paddingVertical: 8
  }
};
