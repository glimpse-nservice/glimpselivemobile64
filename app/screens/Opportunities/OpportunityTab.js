/* eslint-disable react-native/no-inline-styles */
import React, { Component } from "react";
import { PieChart } from "react-native-svg-charts";
import { View, FlatList, Text, ScrollView } from "react-native";
import {
  numberWithCommas,
  formatNumber,
  formatType
} from "../../config/Utility";
import styles from "../Opportunities/OpportunityTabStyles";

export default class OpportunityTab extends Component {
  constructor(props) {
    super(props);

    this.state = { data: null, practiceName: "" };
  }

  componentDidMount() {}
  static getDerivedStateFromProps(props, state) {
    if (props !== null && props.data !== null) {
      return {
        data: props.data[0],
        practiceName: props.practiceName
      };
    }
    return null;
  }
  /**
   * This function renders current and previous data rows for
   * whole data
   * @param {*} rowText text row
   * @param {*} color color to show for current and previous data
   * @param {*} data data value
   */
  renderChartDataRow(rowText, data, color) {
    const { horizontalRow, chartColorIndicatorBox, text, rightValue } = styles;

    data = `$${numberWithCommas(data)}`;

    return (
      <View style={[horizontalRow]}>
        <View
          style={[
            chartColorIndicatorBox,
            { backgroundColor: color, margin: 8 }
          ]}
        />
        <Text style={text}>{rowText}</Text>
        <Text style={rightValue}>{data}</Text>
      </View>
    );
  }

  /**
   * This function renders current and previous data rows
   * for different metrics
   * @param {*} rowText text row
   * @param {*} rowLeftIcon icon to show
   * @param {*} rowRightValue data value
   * @param {*} type data type value
   */
  renderMetrixDataRow(rowText, rowLeftIcon, rowRightValue, type) {
    const { horizontalRow, icon, text, rightValue } = styles;
    rowRightValue = formatType(type, rowRightValue);

    return (
      <View style={[horizontalRow, { paddingHorizontal: 16 }]}>
        <Text style={[icon, { color: "#c0c0c0", margin: 8 }]}>
          {rowLeftIcon}
        </Text>
        <Text style={[text, { paddingVertical: 4, paddingHorizontal: 8 }]}>
          {rowText}
        </Text>
        <Text style={rightValue}>{rowRightValue}</Text>
      </View>
    );
  }
  /**
   * This function renders metrics on the screen
   * @param {metric data} metric
   */
  renderMetrics(metric) {
    const {
      text,
      metrixHeaderText,
      dividerLine,
      icon,
      horizontalRow,
      rightValue
    } = styles;

    const item = metric.item;
    const missedOpp = item.missedOpportunity;
    let colorMissed = "black";
    let missedText = "";
    let missedValue = "";
    switch (missedOpp === 0) {
      case true:
        colorMissed = "#69AD4C";
        missedText = "No Missed Opportunity";
        missedValue = "---";
        break;
      case false:
        colorMissed = "#FF0000";
        missedText = "Missed Opportunity";
        missedValue = `$${numberWithCommas(formatNumber(missedOpp))}`;
        break;
      default:
    }

    return (
      <View>
        <Text style={metrixHeaderText}>{item.metricName}</Text>
        {this.renderMetrixDataRow(
          "Performance",
          "",
          item.performance,
          item.dataType
        )}
        <View style={dividerLine} />
        {this.renderMetrixDataRow(
          "Comparison",
          "",
          item.comparison,
          item.dataType
        )}
        <View style={dividerLine} />

        <View style={[horizontalRow, { paddingHorizontal: 16 }]}>
          <Text style={[icon, { color: colorMissed, margin: 8 }]}></Text>
          <Text
            style={[
              text,
              {
                fontWeight: "500",
                color: colorMissed,
                paddingVertical: 8,
                paddingHorizontal: 8
              }
            ]}
          >
            {missedText}
          </Text>
          <Text style={[rightValue, { color: colorMissed }]}>
            {missedValue}
          </Text>
        </View>
      </View>
    );
  }

  //determine to render chart or not
  renderPieChart(series, categoryName) {
    console.log(series);
    const sliceColor = ["#FF0000", "#69AD4C"];
    const { chartTextStyle } = styles;

    const pieData = [
      { key: "opportunities", value: series[0], svg: { fill: sliceColor[0] } },
      { key: "performance", value: series[1], svg: { fill: sliceColor[1] } }
    ];

    if (series[0] === 0 && series[1] === 0) {
      return (
        <View>
          <Text style={{ fontWeight: "bold", alignSelf: "center", margin: 5 }}>
            No Chart Available
          </Text>
        </View>
      );
    } else {
      return (
        <PieChart data={pieData} style={{ height: 200, marginTop: 10 }}>
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <Text style={chartTextStyle}>{categoryName}</Text>
          </View>
        </PieChart>
      );
    }
  }
  render() {
    const { headerText, mainViewContainerStyle, chartTextStyle } = styles;

    const data = this.state.data;
    const series = [];
    const sliceColor = ["#FF0000", "#69AD4C"];

    let opportunity = null;
    let performance = null;
    let metrics = [];
    let categoryName = "";

    if (data !== null) {
      series.push(data.opportunity);
      series.push(data.performance);

      opportunity = formatNumber(data.opportunity);

      performance = formatNumber(data.performance);

      metrics = data.opportunities;

      categoryName = data.category;
    }

    return (
      <View style={{ flex: 1, backgroundColor: "white", marginTop: 48 }}>
        <ScrollView>
          <Text style={headerText}>{this.state.practiceName}</Text>

          <View style={mainViewContainerStyle}>
            {this.renderPieChart(series, categoryName)}
            {this.renderChartDataRow("Opportunity", opportunity, sliceColor[0])}
            {this.renderChartDataRow(
              "Total Performance",
              performance,
              sliceColor[1]
            )}

            <View
              style={{
                marginTop: 10,
                marginBottom: 10,
                height: 0.5,
                backgroundColor: "#A1A1A1"
              }}
            />

            <FlatList
              data={metrics}
              keyExtractor={metric => metric.metricName}
              renderItem={metric => this.renderMetrics(metric)}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
