export default {
  container: { flex: 1, backgroundColor: 'white', marginTop: 48 },
  mainViewContainerStyle: {
    elevation: 5,
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 5,
    shadowRadius: 14,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0, 0, 0, 0.18)'
  },
  chartTextStyle: {
    color: '#394263',
    fontFamily: 'Roboto-Bold',
    position: 'absolute',
    fontSize: 15
  },
  listContainer: {
    flex: 1,
    backgroundColor: 'white',
    margin: 16,
    borderRadius: 5,
    shadowRadius: 14,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'rgba(0, 0, 0, 0.14)'
  },
  horizontalRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightValue: {
    fontSize: 14,
    color: '#394263',
    position: 'absolute',
    right: 0,
    padding: 24,
    fontFamily: 'Roboto-Bold'
  },
  chartColorIndicatorBox: {
    height: 12,
    width: 12,
    marginLeft: 14,
    borderRadius: 2
  },
  text: {
    fontSize: 15,
    color: '#394263',
    fontFamily: 'Roboto-Regular'
  },
  icon: {
    fontSize: 24,
    marginRight: 16,
    fontFamily: 'icomoon'
  },
  dividerLine: {
    height: 0.5,
    backgroundColor: '#A1A1A1',
    marginHorizontal: 24,
    marginVertical: 16
  },
  headerText: {
    paddingTop: 24,
    paddingHorizontal: 24,
    color: '#394263',
    fontFamily: 'Roboto-Regular',
    fontWeight: '500',
    fontSize: 18
  },
  metrixHeaderText: {
    textAlign: 'center',
    margin: 16,
    color: '#394263',
    fontFamily: 'Roboto-Regular',
    fontWeight: '500',
    fontSize: 16
  }
};
