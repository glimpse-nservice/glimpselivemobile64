/* eslint-disable no-useless-constructor */
import React, { Component } from "react";
import { Text } from "react-native-elements";
import { View, Modal, TouchableHighlight, Alert } from "react-native";
import md5 from "md5";
import styles from "./WizardStyles";
import { UserData } from "../config/UserData";
import Step from "./Step";
import Home from "../screens/Home/Home";
import WebService, {
  URL_USER_SECURITY_QUESTIONS,
  URL_USER_CHECK_SECURITY_ANSWERS,
  URL_UPDATE_PASSWORD,
  URL_USER_SET_UP,
  URL_LOGIN
} from "../webservice";
import { onLogIn } from "../helper/auth";

let errorArray = [];

const { headerText, errorText, buttonStyle } = styles;

export default class Wizard extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    index: 0,
    values: {
      ...this.props.initialValues
    },
    modalVisible: false,
    returnData: [],
    loginId: 0,
    registrationUpdateComplete: false,
    title: this.props.title
  };

  static Step = props => <Step {...props} />;

  nextStep = () => {
    if (this.state.index !== this.props.children.length - 1) {
      if (this.state.title == "NewUserForm") {
        if (this.recheckPassword() == 0) {
          this.setState(prevState => ({
            index: prevState.index + 1
          }));
        }
      }

      if (this.state.title == "UpdatePassword") {
        //check to see if userName is empty and do an alert.

        if (this.state.values.UserName == "") {
          Alert.alert("Error", "User Name is required");
        } else {
          //check to make sure username exist.
          WebService.get(
            URL_USER_SECURITY_QUESTIONS(escape(this.state.values.UserName))
          )
            .then(response => {
              const jsonData = response.data;
              //user exist store info and move on
              UserData.storeUserName(this.state.values.UserName);
              this.setState(prevState => ({
                index: prevState.index + 1
              }));
            })
            .catch(error => {
              console.log(error);
              Alert.alert("Error", "User not Found. Try Again");
            });
        }

        if (this.state.index !== 0) {
          //second step
          if (
            this.state.values.UserSecurityQuestion.question1Answer == "" ||
            this.state.values.UserSecurityQuestion.question2Answer == ""
          ) {
            this.setState(prevState => ({
              index: prevState.index - 1
            }));

            Alert.alert("Error", "Security Questions must be answered");
          } else {
            //check answers
            WebService.get(
              URL_USER_CHECK_SECURITY_ANSWERS(
                this.state.values.UserName.trim(),
                this.state.values.UserSecurityQuestion.question1Answer,
                this.state.values.UserSecurityQuestion.question2Answer
              )
            )
              .then(response => {
                const jsonData = response.data;
              })
              .catch(error => {
                Alert.alert(
                  "Error",
                  "Incorrect answers to the security questions"
                );
                this.setState(prevState => ({
                  index: prevState.index - 1
                }));
              });
          }
        }
      }
    }
  };

  recheckPassword() {
    let error = 0;
    //pasword is empty
    if (
      this.state.values.NewPassword.password == "" &&
      this.state.values.NewPassword.confirmPassword == ""
    ) {
      Alert.alert("Error", "Passwords cannot be empty");
      error = 1;
    }
    //some other error
    else if (this.state.values.NewPassword.errors.length !== 0) {
      errorArray = this.state.values.NewPassword.errors.map(e => {
        return JSON.stringify(e);
      });
      this.setState({ modalVisible: true });
      error = 1;
    }

    return error;
  }

  onValueChange = (name, value) => {
    this.setState(prevState => ({
      values: {
        ...prevState.values,
        [name]: value
      }
    }));
  };

  onSubmit = () => {
    if (this.state.title == "NewUserForm") {
      //questions are  empty
      if (
        this.state.values.SecurityQuestion.question1Answer == "" ||
        this.state.values.SecurityQuestion.question2Answer == ""
      ) {
        Alert.alert("Error", "Security Questions must be answered");
      } else {
        const password = md5(this.state.values.NewPassword.password);
        const securityQuestionId1 = this.state.values.SecurityQuestion
          .question1Id;
        const answer1 = this.state.values.SecurityQuestion.question1Answer;
        const securityQuestionId2 = this.state.values.SecurityQuestion
          .question2Id;
        const answer2 = this.state.values.SecurityQuestion.question2Answer;
        //get userid

        UserData.getUserLoginId()
          .then(loginId => {
            this.setState({ loginId });
            return UserData.getUserName();
          })
          .then(userName =>
            WebService.get(
              URL_USER_SET_UP(
                this.state.loginId,
                userName,
                password,
                securityQuestionId1,
                answer1,
                securityQuestionId2,
                answer2
              )
            ).then(response => {
              const jsonData = response.data;
              this.setState({
                isLoading: false,
                registrationUpdateComplete: true
              });
            })
          )
          .catch(error => {
            console.error(error);
          });
      }
    }

    if (this.state.title == "UpdatePassword") {
      if (this.recheckPassword() == 0) {
        //check answers
        WebService.get(
          URL_UPDATE_PASSWORD(
            this.state.values.UserSecurityQuestion.loginId,
            this.state.values.UserName,
            md5(this.state.values.NewPassword.password)
          )
        )
          .then(response => {
            //log user in
            WebService.get(
              URL_LOGIN.concat(this.state.values.UserName).concat(
                `/${md5(this.state.values.NewPassword.password)}/`
              )
            )
              .then(loginResponse => {
                UserData.storeUserLoginId(loginResponse.data.user.loginId);
                UserData.storeUserName(
                  `${loginResponse.data.user.strUserName}`
                );
                UserData.storeUserPractices(
                  loginResponse.data.dataAccess.practice
                );
                const practices = JSON.parse(loginResponse.data.dataAccess.practice);
                practiceSet = [];
                practices.forEach(value => {
                  const practice = {
                    key: index++,
                    value: value.id,
                    label: value.name,
                    accountTypeId: value.accountTypeId
                  };
                  practiceSet.push(practice);
                })
                UserData.storeUserSelectedPractic(practiceSet[0]);
                UserData.storeUserLocations(
                  loginResponse.data.dataAccess.location
                );
                onLogIn().then(() =>
                  this.setState({
                    isLoading: false,
                    registrationUpdateComplete: true
                  })
                );
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(error => {
            console.log(error);
          });
      }
    }
  };

  render() {
    if (this.state.registrationUpdateComplete == false) {
      return (
        <View style={{ flex: 1 }}>
          {React.Children.map(this.props.children, (el, index) => {
            if (index === this.state.index) {
              return React.cloneElement(el, {
                currentIndex: this.state.index,
                nextStep: this.nextStep,
                isLast: this.state.index === this.props.children.length - 1,
                onChangeValue: this.onValueChange,
                values: this.state.values,
                onSubmit: this.onSubmit,
                title: el.props.title
              });
            }
            return null;
          })}
          <View
            style={{ marginTop: 50, justifyContent: "center", padding: 20 }}
          >
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                this.setState({ modalVisible: false });
              }}
            >
              <View>
                <Text style={headerText}>Correct the Following Errors: </Text>
                {errorArray.map((item, key) => (
                  <Text key={key} style={errorText}>
                    {" "}
                    {item}{" "}
                  </Text>
                ))}
                <TouchableHighlight
                  style={buttonStyle}
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}
                >
                  <Text style={{ color: "#fff" }}>Close</Text>
                </TouchableHighlight>
              </View>
            </Modal>
          </View>
        </View>
      );
    }

    return <Home />;
  }
}
