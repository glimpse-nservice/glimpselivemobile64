/* eslint-disable no-useless-constructor */
import React, { Component } from "react";
import { View } from "react-native";
import { Button } from "react-native-elements";
import styles from "./WizardStyles";

const { buttonWrapper } = styles;
export default class Step extends Component {
  constructor(props) {
    super(props);
  }

  state = {};
  render() {
    return (
      <View>
        {this.props.children({
          onChangeValue: this.props.onChangeValue,
          values: this.props.values
        })}
        {this.props.isLast ? (
          <Button
            title="Submit"
            onPress={this.props.onSubmit}
            clear
            titleStyle={{ color: "white", fontFamily: "Roboto-Bold" }}
            // eslint-disable-next-line react-native/no-inline-styles
            buttonStyle={{
              flexDirection: "row",
              borderRadius: 25,
              height: 50,
              flex: 1
            }}
            containerStyle={buttonWrapper}
          />
        ) : (
          <Button
            title="Next"
            onPress={this.props.nextStep}
            clear
            titleStyle={{ color: "white", fontFamily: "Roboto-Bold" }}
            buttonStyle={{
              flexDirection: "row",
              borderRadius: 25,
              height: 50,
              flex: 1
            }}
            containerStyle={buttonWrapper}
          />
        )}
      </View>
    );
  }
}
