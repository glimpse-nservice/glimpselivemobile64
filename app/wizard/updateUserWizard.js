import React, { Component } from "react";
import { Text } from "react-native-elements";
import { View, Modal, TouchableHighlight, Alert } from "react-native";
import md5 from "md5";
import styles from "./WizardStyles";
import { UserData } from "../config/UserData";
import Step from "./Step";
import Home from "../screens/Home/Home";

let errorArray = [];

const { headerText, errorText, buttonStyle } = styles;

export default class Wizard extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    index: 0,
    values: {
      ...this.props.initialValues
    },
    modalVisible: false,
    returnData: [],
    loginId: 0,
    updateCompleted: false
  };

  static Step = props => <Step {...props} />;

  nextStep = () => {
    if (this.state.index !== this.props.children.length - 1) {
      //pasword is empty
      if (
        this.state.values.NewPassword.password == "" &&
        this.state.values.NewPassword.confirmPassword == ""
      ) {
        Alert.alert("Error", "Passwords cannot be empty");
      }
      //some other error
      else if (this.state.values.NewPassword.errors.length !== 0) {
        errorArray = this.state.values.NewPassword.errors.map(e => {
          return JSON.stringify(e);
        });
        this.setState({ modalVisible: true });
      }
      //no error
      else {
        this.setState(prevState => ({
          index: prevState.index + 1
        }));
      }
    }
  };
  onValueChange = (name, value) => {
    this.setState(prevState => ({
      values: {
        ...prevState.values,
        [name]: value
      }
    }));
  };

  onSubmit = () => {
    //questions are  empty
    if (
      this.state.values.SecurityQuestion.question1Answer == "" ||
      this.state.values.SecurityQuestion.question2Answer == ""
    ) {
      Alert.alert("Error", "Security Questions must be answered");
    } else {
      const password = md5(this.state.values.NewPassword.password);
      const securityQuestionId1 = this.state.values.SecurityQuestion
        .question1Id;
      const answer1 = this.state.values.SecurityQuestion.question1Answer;
      const securityQuestionId2 = this.state.values.SecurityQuestion
        .question2Id;
      const answer2 = this.state.values.SecurityQuestion.question2Answer;

      UserData.getUserLoginId()
        .then(loginId => {
          this.setState({ loginId });
          return UserData.getUserName();
        })
        .then(userName =>
          WebService.get(
            URL_USER_SET_UP(
              this.state.loginId,
              userName,
              password,
              securityQuestionId1,
              answer1,
              securityQuestionId2,
              answer2
            )
          ).then(response => {
            const jsonData = response.data;
            this.setState({ isLoading: false, registrationComplete: true });
          })
        )
        .catch(error => {
          console.error(error);
        });
    }
  };

  render() {
    if (this.state.updateCompleted == false) {
      return (
        <View>
          {console.log(this.props.children)}
          {React.Children.map(this.props.children, (el, index) => {
            if (index === this.state.index) {
              return React.cloneElement(el, {
                currentIndex: this.state.index,
                nextStep: this.nextStep,
                isLast: this.state.index === this.props.children.length - 1,
                onChangeValue: this.onValueChange,
                values: this.state.values,
                onSubmit: this.onSubmit
              });
            }
            return null;
          })}
          <View
            style={{ marginTop: 50, justifyContent: "center", padding: 20 }}
          >
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                this.setState({ modalVisible: false });
              }}
            >
              <View>
                <Text style={headerText}>Correct the Following Errors: </Text>
                {errorArray.map((item, key) => (
                  <Text key={key} style={errorText}>
                    {" "}
                    {item}{" "}
                  </Text>
                ))}
                <TouchableHighlight
                  style={buttonStyle}
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}
                >
                  <Text style={{ color: "#fff" }}>Close</Text>
                </TouchableHighlight>
              </View>
            </Modal>
          </View>
        </View>
      );
    }

    return <Home />;
  }
}
