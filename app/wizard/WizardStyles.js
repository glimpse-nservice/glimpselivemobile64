import Constants from '../config/Constants';

export default {

  headerText: {
    paddingTop: 24,
    paddingBottom: 24,
    paddingHorizontal: 24,
    color: '#394263',
    fontFamily: 'Roboto-Regular',
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',

  },

  errorText: {

    fontFamily: 'Roboto-Regular',
    fontWeight: '500',
    fontSize: 15,
    padding: 15,
    color: 'red'
  },

  buttonStyle: {
    marginTop: 16,
    marginLeft: 24,
    marginRight: 24,
    backgroundColor: Constants.appColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 25
  },

  buttonWrapper: {
    marginTop: 16,
    marginLeft: 24,
    marginRight: 24,
    backgroundColor: Constants.appColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderRadius: 25
  }
}