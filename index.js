if (__DEV__) {
  import("./app/ReactotronConfig").then(() =>
    console.log("Reactotron Configured")
  );
}
import { AppRegistry, Platform } from "react-native";
import KeyboardManager from "react-native-keyboard-manager";
import { name as appName } from "./app.json";
import App from "./app/index";

AppRegistry.registerComponent(appName, () => {
  if (Platform.OS === "ios") {
    KeyboardManager.setToolbarPreviousNextButtonEnable(true);
  }
  return App;
});
